package me.brew;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandExecutor;

public class Spawn implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
    	
  	  if (cmd.getName().equalsIgnoreCase("setspawn")) { 
  		  if (sender instanceof Player) {
  			  if (Main.mysql.adminOrHigher(sender.getName())) {
  				  if (Main.getPlugin().getConfig().getBoolean("spawn-module")) {
  			
  		
  			
  					  Player p = (Player) sender;

  					  Main.getPlugin().getConfig().set("spawn-x", p.getLocation().getX());
  					  Main.getPlugin().getConfig().set("spawn-y", p.getLocation().getY());
  					  Main.getPlugin().getConfig().set("spawn-z", p.getLocation().getZ());
  					  Main.getPlugin().getConfig().set("spawn-yaw", p.getLocation().getYaw());
  					  Main.getPlugin().getConfig().set("spawn-pitch", p.getLocation().getPitch());
  					  Main.getPlugin().saveConfig();
  					  
  					  sender.sendMessage(ChatColor.RED + "Spawn> " + ChatColor.GRAY + "Spawn set!");
             
  					  return true;
             
  				  }
  		
  			
  			
  			  }
  		
  		
  	     	  
  			  else {
  				  
  				  sender.sendMessage(ChatColor.RED + "Spawn> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
  	              return true;  
  			  }
  		 
  		  } 
  		  
  		  else {
  			  
  			  sender.sendMessage(ChatColor.RED + "Spawn> " + ChatColor.GRAY + "Only players can use this command!");
  			  return true;
  		  }
  	  }
	
		return true;
	}
	
	

}
