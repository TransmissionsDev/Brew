package me.brew.event;

import org.bukkit.command.Command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.connorlinfoot.titleapi.TitleAPI;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class StartEvent implements Listener, CommandExecutor  {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		  if (cmd.getName().equalsIgnoreCase("startevent")) {
        	  if (Main.mysql.adminOrHigher(sender.getName()) || !(sender instanceof Player)) {
        		  if (args.length == 0) {
        		
        		sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "You must specify an event name! EG: " + ChatColor.BLUE + "/startevent <EventName>");
				
    			
        		
        		  } else {
        		

        	    if (Main.event) {
        			
        			sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "An event is already running!");
        			return true;
        			
        		}

        		
        		  ArrayList < String > al = new ArrayList < String > ();

                  for (String p: args) {
                      al.add(p);


                  }



                  String eventname = "";

                  for (String p: al) {

                      eventname = eventname + " " + p;

                  }
                 
                  
            
                  Main.eventstoragename = eventname;
                 
            
             
                 for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
                 
               
                	 TitleAPI.sendTitle(p,20,40,60,ChatColor.BLUE + "Event Started »", eventname);
                   	
              
                	 
                
                 }
                 
                 
                 for (Player p : Bukkit.getOnlinePlayers()) {
                    	
             	       
            		  
                	 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
            				if (sender instanceof Player) {
            					Player player = (Player) sender;
            				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has started an event");
            				} else {
            					
            					
            					ActionBarAPI.sendActionBar(p, ChatColor.GRAY + "The CONSOLE has started an event");
                				
            				}
            				 
             		 }
               
            		  
              
                  }    
          		
          		
                 
                 Bukkit.broadcastMessage(ChatColor.BLUE + "Event>" + ChatColor.GRAY +  ChatColor.BOLD + eventname + ChatColor.GRAY + " has started!");
                 
                 Main.event = true;
               
        	
        		
        	}
        	
        	
        	 }
        	  
        else {
        	  
        		 sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
             	return true;
        }
       
        }
		return true;
    
		
	}
	}
