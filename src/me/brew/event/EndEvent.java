package me.brew.event;

import org.bukkit.command.Command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.connorlinfoot.titleapi.TitleAPI;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class EndEvent implements Listener, CommandExecutor  {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		  if (cmd.getName().equalsIgnoreCase("endevent")) {
	        	if (Main.mysql.adminOrHigher(sender.getName()) || !(sender instanceof Player)) {
	        		if (Main.event) {
	        		
	        			Bukkit.broadcastMessage(ChatColor.BLUE + "Event>" + ChatColor.GRAY +  ChatColor.BOLD + Main.eventstoragename + ChatColor.GRAY + " has ended!");
	          
	        			for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
	               
	                
	        				TitleAPI.sendTitle(p,20,40,60,ChatColor.BLUE + "Event Ended »", Main.eventstoragename);
	                  	
	         
	           
	        			}
	            
	        			for (Player p : Bukkit.getOnlinePlayers()) {
	               	
	     	       
	         		  
	        				if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	        					if (sender instanceof Player) {
	         			
	        						Player player = (Player) sender;
	        						ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has ended an event");
	        				
	        					} 
	        					
	        					else {
	         					
	         					
	         					ActionBarAPI.sendActionBar(p, ChatColor.GRAY + "The CONSOLE has ended an event");
	             				
	        					}
	         				
	          		 }
	            
	         		  
	           
	               }    
	      		
	            
	            
	       		
	        			Main.event = false;
	        			
	        			return true;
	        		
	        		}
	                 
	        		else {
	        			sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "An event isn't currently running!");
	        	        return true;    	
	        			
	        		}
	            
	        		
	        		
	        	}
	         	
	         	else {
	          	  
	         		 sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
	            	 return true;
	            }

	        }
		
		return true;
		
	}
	}
