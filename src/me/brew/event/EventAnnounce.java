package me.brew.event;

import org.bukkit.command.Command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class EventAnnounce implements Listener, CommandExecutor  {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		  
        if (cmd.getName().equalsIgnoreCase("eventannounce")) {
        	  if (Main.mysql.adminOrHigher(sender.getName()) || !(sender instanceof Player)) {
        		  if (Main.event) {
        			  if (args.length > 0) {
        		 
        				  ArrayList < String > al = new ArrayList < String > ();

        				  for (String p: args) {
        					  al.add(p);


        				  }



        				  String message = "";
        				  
        				  for (String p: al) {

        					  if (al.indexOf(p) == 0) {
        						  message = message + p;  
                		  
        					  } else {
        						  message = message + " " + p;
        					  
        					  }              	  

        				  }
                 
        		
        		Bukkit.broadcastMessage(ChatColor.BLUE + "\nE§lvent Announcement »\n" + ChatColor.GRAY + message + " \n\n   ");
        	    
        		for (Player p : Bukkit.getOnlinePlayers()) {
                   	
            	       
           		  
        			 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
           				if (sender instanceof Player) {
           					
           					Player player = (Player) sender;
           					ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has made an event announcement");
           				
           				} else {
           					
           					
           					ActionBarAPI.sendActionBar(p, ChatColor.GRAY + "The CONSOLE has made an event announcement");
               				
           				}
           			
            		 }

                 }    	
        		
        	}  
        		
        		else {
        		
        			sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "You must specify a message to announce!");
        	    return true;	
        		
        	}
        		
        	}
        	  else {
        
              sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "An event isn't currently running!");
      	      return true;
        		
        	}
        	 	
        } 
        	else {
        	  
        		 sender.sendMessage(ChatColor.BLUE + "Event> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
             	 return true;
             	 
          }
        }
		
		
		return true;
		
	}
	}
