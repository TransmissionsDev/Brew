package me.brew.chat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class LockChat implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
		
		  
        if (cmd.getName().equalsIgnoreCase("unlockchat")) {
      
        	if (Main.getPlugin().getConfig().getBoolean("lockchat-module")) {
        		  if (Main.mysql.modOrHigher(sender.getName()) || !(sender instanceof Player)) {
        			  if (Main.getPlugin().getConfig().getBoolean("chatlocked")) {
        		
        			Bukkit.broadcastMessage(ChatColor.RED + "Chat> " + ChatColor.GRAY + "The chat has been unlocked!");
        			
        			Main.getPlugin().getConfig().set("chatlocked", null);
        			Main.getPlugin().saveConfig();
        			Main.getPlugin().reloadConfig();
        		
        			  }
        		
        		else {
        			
        			sender.sendMessage(ChatColor.RED + "Chat> " + ChatColor.GRAY + "The chat is already open!");
        			
        		}
        		
        	}
        	
        	else {
          	  
        		 sender.sendMessage(ChatColor.RED + "Chat> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
            	 return true;
            }

        	}
        	
        	
        }
        

        
        if (cmd.getName().equalsIgnoreCase("lockchat")) {
          
        	if (Main.getPlugin().getConfig().getBoolean("lockchat-module")) {
              if (Main.mysql.modOrHigher(sender.getName()) || !(sender instanceof Player)) {
        		if (Main.getPlugin().getConfig().get("chatlocked") == null || !(Main.getPlugin().getConfig().getBoolean("chatlocked"))) {
        	
        		Main.getPlugin().getConfig().set("chatlocked", true);
        		Bukkit.broadcastMessage(ChatColor.RED + "Chat> " + ChatColor.GRAY + "The chat has been locked!");
        		
        		Main.getPlugin().saveConfig();
        		Main.getPlugin().reloadConfig();
        		
        		} else {
        			
        			
        			
        			sender.sendMessage(ChatColor.RED + "Chat> " + ChatColor.GRAY + "The chat is already locked!");
        				
        			
        			
        	    }
        	} 
        	 	else {
                	  
        	 		 sender.sendMessage(ChatColor.RED + "Chat> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
                	 return true;
                  }

           	}
        	
        	
        }
        
 
		
		return true;
	}

	 
    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
    	if (Main.getPlugin().getConfig().getBoolean("lockchat-module") && Main.getPlugin().getConfig().getBoolean("chatlocked")) {
  
    			  if (!(Main.mysql.adminOrHigher(e.getPlayer().getName()))) {
    			
    				  e.setCancelled(true);
    
    			  }
    	}
    	
    }
}