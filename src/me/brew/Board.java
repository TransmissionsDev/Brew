package me.brew;


import org.bukkit.command.Command;



import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class Board implements Listener, CommandExecutor  {
	
	

private static Objective o;
private static Scoreboard board = null;
private static Score score;
private static Boolean alt = false;

public static void clearCustomBoard() {

	  ScoreboardManager manager = Bukkit.getScoreboardManager();
		
	   board = manager.getNewScoreboard();
	 
	   
	   
	   
	   o = board.registerNewObjective(ChatColor.RED + "  §lWelcome", "dummy");
		
	   o.setDisplaySlot(DisplaySlot.SIDEBAR);
	
	   int counter = 0;
	   
	   while (counter != 15) {
		 
		String text = "";
		
		while (text.length() != counter * 2) {
		   text = text + " ";
			   
		}
		
		Score score;
		
		 score = o.getScore(text); 
		 score.setScore(counter);
		  
		counter++; 
		 
	   }
	   
	   
	
}


public static Scoreboard getboard(String playername, boolean join) {	
	if (!alt) {
		if (join) {
	
			return getdefaultboard(playername);
		
		} else {
			
			return getdefaultleaveboard(playername);
			
		}
		
	} else {
		return board;
	}

}


public static Scoreboard getdefaultleaveboard(String playername) {
	   
	   ScoreboardManager manager = Bukkit.getScoreboardManager();
	   
	   Scoreboard board = manager.getNewScoreboard();
	   
	   Objective o;
	   
	   
	   
	   o = board.registerNewObjective(ChatColor.RED + "  §lWelcome", "dummy");
		
	   o.setDisplaySlot(DisplaySlot.SIDEBAR);
	
	    score = o.getScore("              "); 
	    score.setScore(12);
		
	 
		score = o.getScore(ChatColor.RED + "SERVER »"); 
	    score.setScore(11);
		
	    score = o.getScore(Main.getPlugin().getServer().getServerName() + " "); 
	    score.setScore(10);
	    
	    int size = Bukkit.getOnlinePlayers().size() - Main.vanishedplayers;
	   
	    	
		score = o.getScore(size - 1 + ""); 
	    score.setScore(4);
    
	    
	    score = o.getScore("       "); 
	    score.setScore(9);
	   
	    score = o.getScore(ChatColor.AQUA + "RANK »"); 
	    score.setScore(8);
	    

	    String rank = Main.mysql.returnRankPrefix(Main.mysql.getRank(playername));
	    
	    if (rank.equalsIgnoreCase("")) {
	    	
	    	score = o.getScore(ChatColor.GRAY + "None"); 
		    score.setScore(7);
	    } else {
	    
		score = o.getScore(rank); 
	    score.setScore(7);
	    
	    }
	    
	    score = o.getScore("           "); 
	    score.setScore(6);
	    
	    score = o.getScore(ChatColor.GOLD + "ONLINE »"); 
	    score.setScore(5);
	    

	    
	    score = o.getScore("      "); 
	    score.setScore(3);
	    
	    score = o.getScore(ChatColor.GREEN + "DISCORD »"); 
	    score.setScore(2);
	    
	    score = o.getScore("Discord.io/cosmic"); 
	    score.setScore(1);
		return board;
	   
}




public static Scoreboard getdefaultboard(String playername) {
	
	
	
	ScoreboardManager manager = Bukkit.getScoreboardManager();
	   
	   Scoreboard board = manager.getNewScoreboard();
	   
	   Objective o;
	   
	   o = board.registerNewObjective(ChatColor.RED + "  §lWelcome", "dummy");
		
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
	
	    score = o.getScore("              "); 
	    score.setScore(12);
		
	 
		score = o.getScore(ChatColor.RED + "SERVER »"); 
	    score.setScore(11);
		
	    score = o.getScore(Main.getPlugin().getServer().getServerName() + " "); 
	    score.setScore(10);	
	    	
    
	    
	    score = o.getScore("       "); 
	    score.setScore(9);
	   
	    score = o.getScore(ChatColor.AQUA + "RANK »"); 
	    score.setScore(8);

	    String rank = Main.mysql.returnRankPrefix(Main.mysql.getRank(playername));
	    
	    if (rank.equalsIgnoreCase("")) {
	    	
	    	score = o.getScore(ChatColor.GRAY + "None"); 
		    score.setScore(7);
	    } else {
	    
		score = o.getScore(rank); 
	    score.setScore(7);
	    
	    }

	    
	    score = o.getScore("           "); 
	    score.setScore(6);
	    
	    score = o.getScore(ChatColor.GOLD + "ONLINE »"); 
	    score.setScore(5);
	    
		score = o.getScore(Bukkit.getOnlinePlayers().size() - Main.vanishedplayers + ""); 
	    score.setScore(4);
	    
	    score = o.getScore("      "); 
	    score.setScore(3);
	    
	    score = o.getScore(ChatColor.GREEN + "DISCORD »"); 
	    score.setScore(2);
	    
	    score = o.getScore("Discord.io/cosmic"); 
	    score.setScore(1);
		return board;
	   
}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		
		if (cmd.getName().equalsIgnoreCase("board")) {
		if (sender instanceof Player) {
			if (Main.mysql.adminOrHigher(sender.getName())) {
					if (args.length == 0) {
					
						sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "You did not specify a valid sub-command! Run /board help to learn more.");
						return true;
				
					}
					
					if (args[0].equalsIgnoreCase("help")) {
					sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "Board commands:\n\n" + 
					ChatColor.RED + "/board default " + ChatColor.GRAY + "| Shows the defualt board to all online users on the server\n" + 
					ChatColor.RED + "/board custom " + ChatColor.GRAY + "| Shows the custom board for all online users and shows them the default board\n" + 
					ChatColor.RED + "/board set <Position> <Text>" + ChatColor.GRAY + " | Sets text at a specified position\n" + 
					ChatColor.RED + "/board reset" + ChatColor.GRAY + " | Resets the board\n" + 
					ChatColor.RED + "/board clear" + ChatColor.GRAY + " | Clears a specified line on the board\n");
					
					return true;
					}
				
					if (args[0].equalsIgnoreCase("default")) {
						if (alt) {
							
						alt = false;
				  
						for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
			    	    
							player.setScoreboard(getboard(player.getName(), true));
			            
						}
						
						sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "The default board is now active.");
						return true;
						
						} else {
							
						sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "The default board is already showing! You can show the custom board by running /board show!");	
						return true;
						}
					}
		
				
					if (args[0].equalsIgnoreCase("custom")) {
						
					
						if (!alt) {
					    
						if (board == null) {
					    
							clearCustomBoard();
					    	
					    }
							
						alt = true;
						
						for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
			    	    
			            player.setScoreboard(getboard(player.getName(), true));
			            
						}
					
						
						sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "The custom board is now shwoing. You can make edits to it by running /board set <Position> <Text>.");
						return true;
						
						} else {	
						
							sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "The custom board is already showing! To show the default board, run /board default!");
							return true;
								
						}
				
					
					
						
					}
					
					if (args[0].equalsIgnoreCase("reset")) {
						
						 	clearCustomBoard();
						 	
			    		   if (alt) {
			            		
			                	for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
						    	    
						            player.setScoreboard(getboard(player.getName(), true));
						            
						           }
			                	
			                }
			            	 
			    		   sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "Custom board content reset! Add content to it by running /board set <Position> <Text>.");
			    		   return true;
			            	   
			               }
				    	    
			    		  
					
					
					if (args[0].equalsIgnoreCase("clear")) {
					
						if (args.length < 2) {
							sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "You must specify a line number to clear! EG: /board clear <Line Number>.");
							return true;
						}
						
						if (board == null) {
						
							clearCustomBoard();
								
							
						}
						
						
						
						if (StringUtils.isNumeric(args[1])) {
						
						int line = Integer.parseInt(args[1]);
						
						if (line > 15 || line < 0) {
							sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "You must specify a number 1 - 15!");
							return true;
						}
						
						String text = "";
	
						while (text.length() != line * 2) {
							   text = text + " ";
								   
						}
							
						
						for (String name : o.getScoreboard().getEntries()) {
							if(o.getScore(name).getScore() == line) {
								
								 Score score;
								 board.resetScores(name);
								 score = o.getScore(text); 
								 score.setScore(line);	
								
							}
							
						}
					
							  

					
						  
							
							
						} else {
							
							sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + args[1] + " is not a valid number!");
							return true;	
							
						}
					 
					 	
						   if (alt) {
			            		
			                	for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
						    	    
						            player.setScoreboard(getboard(player.getName(), true));
						            
						           }
			                	
			                }
		            	 
						   sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "Line cleared!");
			    		   return true;
		            	   
		               }
			    	    
		    		 
				
					
		
					
				    if (args[0].equalsIgnoreCase("set")) {
				    	ArrayList < String > list = new ArrayList < String > ();
				    	
				    	if (args.length == 2) {
			            	   
				    		sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "You did not specify a replacement.");
				    		return true;
			            }
						
		                for (String p: args) {
		                    list.add(p);
		                    list.add(" ");


		                }
		               
		                list.remove(0);
		                list.remove(0);
		                list.remove(0);
		                list.remove(0);
		                list.remove(list.size() - 1);

		      

		                String msg = "";

		                for (String p : list) {
		                	
		                    msg = msg + p;

		                }
		                
		            
		                if (board == null) {
		                	
		                	clearCustomBoard();
		                	
		                }
		            	
		            	if (!StringUtils.isNumericSpace(args[1])) {  		
		                 
		            		sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + args[1] + " is not a valid number.");
		            		return true;
						}
		            	
		            	HashMap<String, Integer> texts = new HashMap<String, Integer>();
		            	int num = Integer.parseInt(args[1]);
		            	for (String name : o.getScoreboard().getEntries()) {
		            
		            	if (o.getScore(name).getScore() == num) {
		            		
		            		texts.put(name, o.getScore(name).getScore());
		            	}
		            	}
		            	
		            	
		            	for (String m : texts.keySet()) {
		            
		            	board.resetScores(m);
		            	score = o.getScore(msg.replaceAll("&", "§")); 
				        score.setScore(texts.get(m));
		            	
		            	}
		            	
		            	
		            	 
		         	   if (alt) {
		            		
		                	for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
					    	    
					            player.setScoreboard(getboard(player.getName(), true));
					            
					           }
		                	
		                }		            	 
		          
		         	   
		         	  sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "Replaced.");
		         	  return true;
		            	   
		               }
				    	

					sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "You did not specify a valid sub-command! Run /board help to learn more.");
					return true;

				
			} else {
				
				sender.sendMessage(ChatColor.RED + "Board> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
				return true;	
				
			}
				    
				

			} else {

				sender.sendMessage(ChatColor.RED + "Board>  " + ChatColor.GRAY + "Only players can use this command!");
				return true;
				
			}

		} 
		
		return true;

	
	
		
	}

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {  
     if (Main.mysql.isVanished(e.getPlayer().getName())) {
    	 
    	  e.getPlayer().setScoreboard(getboard(e.getPlayer().getName(), true)); 
    	  return;
     }
     
    	for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
    	    
            player.setScoreboard(getboard(player.getName(), true));
            
           }
   
    	
    }
	
    @EventHandler
    public void onLeave(PlayerQuitEvent e) {   
   if (!(Main.mysql.isVanished(e.getPlayer().getName()))) {
    for (Player player : Main.getPlugin().getServer().getOnlinePlayers()) {
	    
        player.setScoreboard(getboard(player.getName(), false));
        
       }
   }
    }

	}
