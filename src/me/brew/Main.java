package me.brew;

import org.bukkit.event.EventHandler;





import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import com.google.common.io.ByteArrayDataOutput;
import me.brew.punish.*;
import me.brew.utils.CommandSpy;
import me.brew.utils.Gamemode;
import me.brew.utils.PlayerInfo;
import me.brew.utils.StaffChat;
import me.brew.utils.TP;
import me.brew.utils.Vanish;
import me.brew.assistance.Assistance;
import me.brew.assistance.AssistanceReply;
import me.brew.authentication.Authentication;
import me.brew.chat.ClearChat;
import me.brew.chat.LockChat;
import me.brew.database.MySQL;
import me.brew.database.Prefix;
import me.brew.database.Rank;
import me.brew.event.EndEvent;
import me.brew.event.EventAnnounce;
import me.brew.event.StartEvent;
import me.brew.inventory.OpenInv;
import me.brew.inventory.ViewInvs;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.*;
import org.bukkit.entity.Player;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;



public class Main extends JavaPlugin implements Listener  {
	
	public static int vanishedplayers;

	private static Plugin plugin;
	
	public static MySQL mysql;

	public static boolean event;
	
	public static HashMap<String, String> auth;
	
	public static String eventstoragename;
	public static ArrayList < String > viewinvs = new ArrayList < String > ();
    public static ArrayList < String > commandspies = new ArrayList < String > ();	
   
    public static void sendPluginMsg(Player p, ByteArrayDataOutput out) {
		   
		   p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
	  
	   }
	   
	   public void setupSupplyModule() {
			  
		   if (getConfig().getBoolean("supply-module")) {
		        getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() { 
		        	
		        	public void run() {
		           
		        		if (Math.random() * 10 > 4) {
		        		
		        		getServer().broadcastMessage(ChatColor.BLUE + "Supplies> " + ChatColor.GRAY + "A health crate has been supplied from mission control. All cosmonauts will be provided with a random amount of health.");
		        		for (Player p : getServer().getOnlinePlayers()) {
		        			Double health = p.getHealth();
		        			
		        		    p.setHealth( health + (int) Math.random() * 20);
		        			
		        		}
		        		
		        	} else {
		        		
		        		getServer().broadcastMessage(ChatColor.BLUE + "Supplies> " + ChatColor.GRAY + "A food crate has been supplied from mission control. All cosmonauts will be provided with a random amount of food.");
		        		for (Player p : getServer().getOnlinePlayers()) {
		        			int food = p.getFoodLevel();
		        			
		        		    p.setHealth( food + (int) Math.random() * 20);
		        			
		        		}
		        		
		        	}
		        		
		        	}
		         	
		       }, 0, getConfig().getInt("supply-tics"));
		      }

	   
	   }

	  
	   
	 

	   public void setupConfig() {
		   
	       
		    getConfig().options().copyDefaults(true);
		    
		    getConfig().addDefault("sql-host", "none");
		    getConfig().addDefault("sql-user", "none");
		    getConfig().addDefault("sql-pass", "none");
	        getConfig().addDefault("sql-db", "none");
		    getConfig().addDefault("2fa-module", true);
		    getConfig().addDefault("exec-uuid", "someuuid");
		    getConfig().addDefault("supply-module", false);
		    getConfig().addDefault("supply-tics", 4800);
		    getConfig().addDefault("tempban-module", true);
	        getConfig().addDefault("ban-module", true);
	        getConfig().addDefault("actionbar-logs", true);
	        getConfig().addDefault("warn-module", true);
	        getConfig().addDefault("lockchat-module", true); 
	        getConfig().addDefault("firstjoinspawn-module", false); 
	        getConfig().addDefault("doublejump-module", true);
	        getConfig().addDefault("join-msg", true);
	        getConfig().addDefault("leave-msg", true);
	        getConfig().addDefault("spawn-module", true); 
	        getConfig().addDefault("kickondisable-module", true); 
	        getConfig().addDefault("spawn-x", 0);
	        getConfig().addDefault("spawn-y", 0);
	        getConfig().addDefault("spawn-z", 0);
	        getConfig().addDefault("spawn-pitch", 0);
	        getConfig().addDefault("spawn-yaw", 0);
	        getConfig().addDefault("fall-tp", true);
	        getConfig().addDefault("fall-tp-threshold", 6);


	        saveConfig();
	        
	      
	   }
	   
	   
	   
	   public static void sendMessageToEveryoneBut(String playername, String message) {
		   
		   for (Player p : plugin.getServer().getOnlinePlayers()) {
			   
			   if (!(p.getName().equalsIgnoreCase(playername))) {
				   
				   

				   
				   p.sendMessage(message);
				   
			   
			   
		   }
	   }
   }
   
	
	
	   public static void registerEvents(Listener... listeners) {
			for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
			}
			
		}
	
	public void onEnable() {

		 
		   
		      setupConfig();

			   getServer().getLogger().info("[Brew] " + "Connecting to mysql.....");
				 
				
		   
		   mysql = new MySQL(getConfig().getString("sql-host"), getConfig().getString("sql-user"), getConfig().getString("sql-pass"), getConfig().getString("sql-db"));
		   
		   
		   
		   
		   setupSupplyModule();
		    
		    
		    for (Player p : getServer().getOnlinePlayers()) {
		    	  
		    	 com.nametagedit.plugin.NametagEdit.getApi().setNametag(p,"§7" + mysql.getPrefix(p.getName()), "");
				
		    	 p.setPlayerListName("§7" + mysql.getPrefix(p.getName()) + p.getName());
		    	 
		    }
		   
		    
		      
	    
	    getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");   
	     
	    
		   
	    auth = new HashMap<String, String>();
	    
	    
	    
        for (String str : getConfig().getStringList("authentication")) {
            String[] words = str.split(":");
            auth.put(words[0], words[1]);
          
        }
        
        getConfig().set("authentication", null);
	    
		plugin = this;
		
		getCommand("assistance").setExecutor(new Assistance());
		getCommand("assistancereply").setExecutor(new AssistanceReply());
		getCommand("lockchat").setExecutor(new LockChat());
		getCommand("unlockchat").setExecutor(new LockChat());
		getCommand("rank").setExecutor(new Rank());
		getCommand("setspawn").setExecutor(new Spawn());
		getCommand("prefix").setExecutor(new Prefix());
		getCommand("endevent").setExecutor(new EndEvent());
		getCommand("startevent").setExecutor(new StartEvent());
		getCommand("eventannounce").setExecutor(new EventAnnounce());
		getCommand("clearchat").setExecutor(new ClearChat());
		getCommand("warn").setExecutor(new Warn());
		getCommand("ban").setExecutor(new Ban());
		getCommand("tempban").setExecutor(new TempBan());
		getCommand("unban").setExecutor(new Unban());
		getCommand("staffchat").setExecutor(new StaffChat());
		getCommand("tp").setExecutor(new TP());
		getCommand("gamemode").setExecutor(new Gamemode());
		getCommand("kick").setExecutor(new Kick());
		getCommand("openinv").setExecutor(new OpenInv());
		getCommand("viewinvs").setExecutor(new ViewInvs());
		getCommand("vanish").setExecutor(new Vanish());
		getCommand("playerinfo").setExecutor(new PlayerInfo());
		getCommand("commandspy").setExecutor(new CommandSpy());
		getCommand("board").setExecutor(new Board());
		getCommand("auth").setExecutor(new Authentication());
		
		registerEvents(this, new LockChat(), new Board(), new Authentication());
			
		}
		 
		public void onDisable() {
			if (getConfig().getBoolean("kickondisable-module")) {
			Bukkit.broadcastMessage(ChatColor.RED + "System> " + ChatColor.GRAY + "Server shutting down! All players will be kicked!");
		
			for (Player p : getServer().getOnlinePlayers()) {
				
				p.kickPlayer(ChatColor.RED + "The server you were connected to has shutdown. \n\n" + ChatColor.GRAY + getServer().getServerName() + " - " + new Date(System.currentTimeMillis()).toString());
					
			}
			}
		
			
		    List<String> s = getConfig().getStringList("authentication");
            
            for (String p : auth.keySet()) {
                    s.add(p +  ":"  +  auth.get(p));
            }
           
            getConfig().set("authentication", s);
            saveConfig();
			
			plugin = null; //To stop memory leaks
		 
		}
		 
		 
		//To access the plugin variable from other classes
		public static Plugin getPlugin() {
	
	    return plugin;
		
		}
		
	    
	    @SuppressWarnings("deprecation")
		@EventHandler
	    public void onPlayerJoin(PlayerJoinEvent e) {
	    	
	    	Boolean vanished = Main.mysql.isVanished(e.getPlayer().getName());
	
	    	if (vanished) {	
	    
	    		vanishedplayers++;	
	    		com.nametagedit.plugin.NametagEdit.getApi().setNametag(e.getPlayer(), Main.mysql.getPrefix(e.getPlayer().getName()), ChatColor.BLUE + " §l[HIDDEN]");
	    		e.getPlayer().setPlayerListName(ChatColor.STRIKETHROUGH + Main.mysql.getPrefix(e.getPlayer().getName()) + e.getPlayer().getName() + ChatColor.BLUE + " §l[HIDDEN]");
	 			
	    	   e.getPlayer().setGameMode(GameMode.SPECTATOR);

			    if (Main.getPlugin().getConfig().getBoolean("join-msg")) {	
				
				e.setJoinMessage(""); 
				if (!(Main.mysql.adminOrHigher(e.getPlayer().getName()))) {
					e.getPlayer().sendMessage(ChatColor.AQUA + "Hidden Join> " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + e.getPlayer().getName());
					
				}
				for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
			    if (Main.mysql.adminOrHigher(p.getName())) {
			    	
			    	p.sendMessage(ChatColor.AQUA + "Hidden Join> " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + e.getPlayer().getName());

			    }
			    }
			    }

			  
			 
	    	} else {
				
			    if (Main.getPlugin().getConfig().getBoolean("join-msg")) {	
				
				e.setJoinMessage(ChatColor.AQUA + "Join> " + ChatColor.YELLOW + e.getPlayer().getName());

			    }

			  e.getPlayer().setPlayerListName("§7" + Main.mysql.getPrefix(e.getPlayer().getName()) + e.getPlayer().getName());
			 
				com.nametagedit.plugin.NametagEdit.getApi().setNametag(e.getPlayer(), "§7" + Main.mysql.getPrefix(e.getPlayer().getName()), "");
			  
	    	}
				
	    		
	    	if (vanished) {
	    	    
	    		for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
	    		
	    		if (p.getName() != e.getPlayer().getName() && !(Main.mysql.adminOrHigher(p.getName()))) {
	    		
	    		
	    		
	    					p.hidePlayer(e.getPlayer());
	    			
	    				
	    				
	    			
	    			
	    		}
	    		
	    	}
	    	
	    	
	    	}
	    	
	    	for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
	    		
	    		if (p.getName() != e.getPlayer().getName()) {
	    			if (Main.mysql.isVanished(p.getName()) && !(Main.mysql.adminOrHigher(e.getPlayer().getName()))) {
	    					e.getPlayer().hidePlayer(p);	
	    			}
	    			
	    		}
	    		
	    	}
	    	
	    	
			if (getConfig().getBoolean("spawn-module")) {
  				
    		      Player player = (Player) e.getPlayer();
                
    		      player.teleport(new Location(Bukkit.getWorld("world"), getConfig().getDouble("spawn-x"), getConfig().getDouble("spawn-y"), getConfig().getDouble("spawn-z"), getConfig().getInt("spawn-yaw"), getConfig().getInt("spawn-pitch")));
                
    		 }
    				
    				if (getConfig().getBoolean("firstjoinspawn-module")) {
        				
          		      Player player = (Player) e.getPlayer();
                      
          		      OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getName());
          		      
          		      if (!(offlinePlayer.hasPlayedBefore())) {
          		      
          		      
          		      player.teleport(new Location(Bukkit.getWorld("world"), getConfig().getDouble("spawn-x"), getConfig().getDouble("spawn-y"), getConfig().getDouble("spawn-z"), getConfig().getInt("spawn-yaw"), getConfig().getInt("spawn-pitch")));
                      
          				}
    				}
    				
   
		

			   
			
	    }
	    
	    @EventHandler
	    public void onLeave(PlayerQuitEvent e) {  
	  
	    
	    
			
			e.getPlayer().sendMessage(vanishedplayers + "");
	    	e.setQuitMessage("");
	    
	    	if (getConfig().getBoolean("leave-msg")) {
	    	
	    		if (Main.mysql.isVanished(e.getPlayer().getName())) {
	    		vanishedplayers--;
	    		if (!(Main.mysql.adminOrHigher(e.getPlayer().getName()))) {
	    		e.getPlayer().sendMessage(ChatColor.RED + "Hidden Quit> " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + e.getPlayer().getName());

			}	
	    	
			for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
			    if (Main.mysql.adminOrHigher(p.getName())) {
			    	
			    	p.sendMessage(ChatColor.RED + "Hidden Quit> " + ChatColor.YELLOW + ChatColor.STRIKETHROUGH + e.getPlayer().getName());

			    }
			    }
			

			
	    	} else {
	    		
	    		e.setQuitMessage(ChatColor.RED + "Quit> " + ChatColor.YELLOW + e.getPlayer().getName());
	    	    
	    	}
	    
	    }
	    
	    }
	
	    	
	    	
	    @EventHandler
	    public void onMove(PlayerMoveEvent event) {
	    	
	    	if (getConfig().getBoolean("fall-tp") && event.getPlayer().getLocation().subtract(0,1,0).getBlock().getType() == Material.AIR) {
	 

	    	  
		    	for (Player player: Bukkit.getOnlinePlayers()) { 
		            if (player.getLocation().getY() < getConfig().getDouble("spawn-y") - getConfig().getDouble("fall-tp-threshold")) {
		            
		            	 player.teleport(new Location(Bukkit.getWorld("world"), getConfig().getDouble("spawn-x"), getConfig().getDouble("spawn-y"), getConfig().getDouble("spawn-z"), getConfig().getInt("spawn-yaw"), getConfig().getInt("spawn-pitch")));
		                 
		            	
		            
		            }
		        
	    	}
	       }
	    	
	    	if (getConfig().getBoolean("doublejump-module")) {
	        Player p = event.getPlayer();

	        
	        	if (p.getGameMode() != GameMode.CREATIVE && (p.getLocation().subtract(0,1,0).getBlock().getType() != Material.AIR) && (!p.isFlying()))
	        p.setAllowFlight(true);
	        	 
	    
	    	}
	        }
	    
	    @EventHandler
	    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent e) {  
	    	
	    
	    	
	        String type = mysql.getBanType(e.getName());
	        
				if (type != null) {

					if (type.equalsIgnoreCase("perm")) {
					
						String reason = mysql.getBanReason(e.getName());
						
						if (reason.equalsIgnoreCase("")) {
						
							e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ChatColor.RED + "\n\nYou have been PERMANTLY banned from this server!\n\n" + ChatColor.GRAY + "No reason given.");
				
					
						
						
						} 
						
						else {
						    
						
	                	
						
							e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ChatColor.RED + "\n\nYou have been PERMANTLY banned from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason);


						}
	              
					  
						
					}
					
					
					
					if (type.equalsIgnoreCase("temp")) {
					
						String reason = mysql.getBanReason(e.getName());
						
				
						if (reason.equalsIgnoreCase("")) {
							long duration = mysql.getBanDuration(e.getName());
		                  
	                    long now = System.currentTimeMillis();

	                    long diff = duration - now;



	                    if (diff > 0) {
	                     
	                    	
	                    	

	                  

	                        Date expiration = new Date(duration);
							
	                        e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ChatColor.RED + "\n\nYou have been temporarily banned from this server!\n\n" + ChatColor.GRAY + "Unban Date: " + ChatColor.AQUA + ChatColor.UNDERLINE + expiration.toString());

	                    } else {
	                    	
	                    	mysql.unbanPlayer(e.getName());
	                    	
	                    }

	              
						} 
						else {
							
							long duration = mysql.getBanDuration(e.getName());
		                  
	                    long now = System.currentTimeMillis();

	                    long diff = duration - now;



	                    if (diff > 0) {
	                     
	                    	
	                    	

	                 

	                        Date expiration = new Date(duration);
	                       
	                      
	                        e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_BANNED, ChatColor.RED + "\n\nYou have been temporarily banned from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason + ChatColor.GRAY + "\nUnban Date: " + ChatColor.AQUA + ChatColor.UNDERLINE + expiration.toString());

	                    } else {
	                    	
	                      	mysql.unbanPlayer(e.getName());
	                    	
	                    }

						}
					  
						
					}

				}
	    	
	    }
	    
	    @EventHandler
	    public void doubleJump(PlayerToggleFlightEvent e) {
	 
	    	if (getConfig().getBoolean("doublejump-module")) {
	    	
	    	Player p = e.getPlayer();
	     
	        if (p.getGameMode() == GameMode.CREATIVE) return;
	        e.setCancelled(true);
	        p.setAllowFlight(false);
	        p.setFlying(false);
	        p.setVelocity(p.getLocation().getDirection().multiply(1.5).setY(1));
	        
	    	}
	    	  
	    }
	    
	    @EventHandler
	    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
	    	
	    		e.setFormat("§7" + Main.mysql.getPrefix(e.getPlayer().getName()) + "%s" + "§r » " + "%s");	
	    	
	    
	    }
		
	
}

