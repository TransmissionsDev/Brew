package me.brew.utils;


import org.bukkit.command.Command;



import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;

import me.brew.Board;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class Vanish implements Listener, CommandExecutor  {

	public static void vanishplayer(Player target, Player player) {
	
		
	target.setGameMode(GameMode.SPECTATOR);
		
		  
  	  for (Player p : Bukkit.getOnlinePlayers()) {
      	
  		  if (Main.mysql.adminOrHigher(p.getName())) {
      		if (Main.getPlugin().getConfig().getBoolean("actionbar-logs"))	{
      			 ActionBarAPI.sendActionBar(p, target.getPlayerListName() + ChatColor.GRAY + " has been hidden by " + player.getPlayerListName());
      		}
      		} else {
  		  
  		  p.hidePlayer(target);
  		  
      	  }
  		  
  	
  		 }
  	  

  	  com.nametagedit.plugin.NametagEdit.getApi().setNametag(player, ChatColor.STRIKETHROUGH + Main.mysql.getPrefix(player.getName()), "");
  	  
  	  
  	  Main.mysql.vanish(target.getName().toLowerCase());
  	  Main.vanishedplayers++;
	     for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
   	    
           p.setScoreboard(Board.getboard(p.getName(), true));
           
         }
  

	     if (Main.getPlugin().getConfig().getBoolean("leave-msg")) {	
				
	    	  Main.sendMessageToEveryoneBut(target.getName(), ChatColor.RED + "Quit> " + ChatColor.YELLOW + target.getName());

	    }

	     target.setPlayerListName(ChatColor.STRIKETHROUGH + Main.mysql.getPrefix(target.getName()) + target.getName() + ChatColor.BLUE + " §l[HIDDEN]");
	     com.nametagedit.plugin.NametagEdit.getApi().setNametag(target, Main.mysql.getPrefix(target.getName()), ChatColor.BLUE + " §l[HIDDEN]");
 			 
	    

 	
 	  

	     target.sendMessage(ChatColor.RED + "Vanish> " + ChatColor.GRAY + "You're now " + ChatColor.RED + ChatColor.BOLD + "INVISIBLE" + ChatColor.GRAY + " to all players except or users with the " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " rank or higher.");
		
	} 
	
	public static void unvanishplayer(Player target, Player player) {

  	  
	  target.setGameMode(GameMode.SURVIVAL);
  	 
  	  Main.mysql.unvanish(target.getName());
  	  Main.vanishedplayers--;
  	  com.nametagedit.plugin.NametagEdit.getApi().setNametag(target, Main.mysql.getPrefix(target.getName()), "");
		  
  	  
  	     for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
  	    	    
  	            p.setScoreboard(Board.getboard(p.getName(), true));
  	            
  	          }
  	   
    
  	   
  	     
 		
		    if (Main.getPlugin().getConfig().getBoolean("join-msg")) {	
			
		    	  Main.sendMessageToEveryoneBut(target.getName(), ChatColor.AQUA + "Join> " + ChatColor.YELLOW + target.getName());

		    }

		    target.setPlayerListName("§7" + Main.mysql.getPrefix(target.getName()) + target.getPlayer().getName());
		 
			com.nametagedit.plugin.NametagEdit.getApi().setNametag(target, "§7" + Main.mysql.getPrefix(target.getName()), "");
		  
		  	  for (Player p : Bukkit.getOnlinePlayers()) {
		        	
		  		  p.showPlayer(target);
		  		  
		  		  if (Main.mysql.adminOrHigher(p.getName())) {
		  			  if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
		   			 ActionBarAPI.sendActionBar(p, target.getPlayerListName() + ChatColor.GRAY + " has been un-hidden by " + player.getPlayerListName());
		  			  }
		   		 }
		     
		  		  
		    
		        }    
		  	  

		      target.sendMessage(ChatColor.RED + "Vanish> " + ChatColor.GRAY + "You're now " + ChatColor.GREEN + ChatColor.BOLD + "VISIBLE" + ChatColor.GRAY + " to all players!");
		  	


		
	} 
	

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		 
		if (cmd.getName().equalsIgnoreCase("vanish")) { 
	        	  if (sender instanceof Player) {
	    			  if (Main.mysql.modOrHigher(sender.getName())) { 
	    				  Player player = (Player) sender;
	    				 
	    				  if (args.length > 0) {
	    					  Player target = Main.getPlugin().getServer().getPlayer(args[0]);
	    					  if (target == null) {
	    						  
	    						  sender.sendMessage(ChatColor.RED + "Vanish> " + ChatColor.GRAY + args[0] + "is not an online player!");
	    						  return true;
	    					  }
	    					 
	    					  if (Main.mysql.isVanished(target.getName())) {
	    	        			  
	    						unvanishplayer(target, player);
	    					
	    	         			   
	    	        		  } else {
	    	        			  
	    	        			vanishplayer(target, player);		
	    	        		
	    	        		  }
	    	        		 
	    			
	    				  }
	    				  
	    				  if (args.length == 0) {
	        		  
	        		  if (Main.mysql.isVanished(sender.getName())) {
	        			  
	        			    
	        			  unvanishplayer(player, player);
	         			   
	        		  } else {
	        			  
	        		
	        			  vanishplayer(player, player);
	        			
	        		  }
	        		 
	        		  
	        	  }
	    			  } else {
	    				  sender.sendMessage(ChatColor.RED + "Vanish> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
	    	              return true;   
	    				  
	    			  }
	    			  
	        	  } else {
	        		  
	        		  sender.sendMessage(ChatColor.RED + "Vanish> " + ChatColor.GRAY + "Only players can use this command!");
	    			  return true; 
	        	  }
	        	  
	        	  
	          }
	          

		return true;
		
	}
	

	}
