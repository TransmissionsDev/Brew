package me.brew.utils;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.CommandExecutor;

public class CommandSpy implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("commandspy")) {
			if (sender instanceof Player) {
				if (Main.mysql.adminOrHigher(sender.getName())) {

					if (Main.commandspies.contains(sender.getName().toLowerCase())) {
						
						Main.commandspies.remove(sender.getName().toLowerCase());
						sender.sendMessage(ChatColor.RED + "CommandSpy> " + ChatColor.GRAY + "You have disabled CommandSpy.");
						return true;
					} else {
						
						Main.commandspies.add(sender.getName().toLowerCase());
						sender.sendMessage(ChatColor.RED + "CommandSpy> " + ChatColor.GRAY + "You have enabled CommandSpy.");
						return true;
						
					}

				} else {

					sender.sendMessage(ChatColor.RED + "CommandSpy> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
					return true;

				}

			} else {

				sender.sendMessage(ChatColor.RED + "CommandSpy> " + ChatColor.GRAY + "Only players can use this command!");
				return true;

			}

		}
		return true;
		
	}
	}
