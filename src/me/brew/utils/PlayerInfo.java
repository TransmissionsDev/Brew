package me.brew.utils;


import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;



import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.CommandExecutor;

public class PlayerInfo implements Listener, CommandExecutor  {

	public static void sendBanInfo(String subject, Player request) {
		String type = Main.mysql.getBanType(subject);
		
		if (type == null) {
			
			request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Banned: " + ChatColor.RED + "FALSE");
			
		} else {
			
			if (type.equalsIgnoreCase("temp")) {
				long duration = Main.mysql.getBanDuration(subject);
				
				long now = System.currentTimeMillis();

                long diff = duration - now;
                
                String banner = Main.mysql.getBanner(subject);



                if (diff > 0) {
                	Date expiration = new Date(duration);
                	request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Banned: " + ChatColor.GREEN + "TRUE");
                	request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Type: " + ChatColor.RED + "TEMPORARY");
                	request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Unban Date: " + ChatColor.GREEN + expiration.toString());
                	request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Prosecuter: " + ChatColor.RED + banner);
                    
                } else {
                	
                	Main.mysql.unbanPlayer(subject);
                	request.sendMessage(ChatColor.RED + "Banned: " + ChatColor.GRAY + "FALSE");
                	
                }
                	
			} else {
			   
				String banner = Main.mysql.getBanner(subject);
				
				request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Banned: " + ChatColor.GREEN + "TRUE");
            	request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Type: " + ChatColor.RED + "PERMANENT");
            	request.sendMessage(ChatColor.RED + subject + "> " +  ChatColor.GRAY + "Prosecuter: " + ChatColor.GREEN + banner);
                
				
			}
		}
		 
	}
	
	

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("playerinfo")) { 
  	      if (sender instanceof Player) {
  	    	  if (Main.mysql.helperOrHigher(sender.getName())) {
  	    	
  	    		  
  	    		OfflinePlayer target;
  	    		  
  	    		if (args.length == 0) {
  	    		
  	    		 target = Bukkit.getOfflinePlayer(sender.getName());
  	    		
  	    		} else {
  	    		
  	    		 target = Bukkit.getOfflinePlayer(args[0]);
  	    
  	    		}
  	    		
  	    	
  	    		sender.sendMessage(ChatColor.RED + "PlayerInfo> " + ChatColor.GRAY + "Retriving playerdata for " + ChatColor.RED + target.getName() + ChatColor.GRAY + "...");
          		sender.sendMessage("  ");
          	      
  	    		
  	    		
  	    		Player player = Main.getPlugin().getServer().getPlayer(sender.getName());   
  	    		
          		
          		String rank = Main.mysql.returnRankPrefix(Main.mysql.getRank(target.getName()));
          
          			sendBanInfo(target.getName(), player);
          			
          			if (!target.isOnline()) {
          			
          			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Online: " + ChatColor.RED + "FALSE");
          			
          			} else {
          				
          			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Online: " + ChatColor.GREEN + "TRUE");
              				
          			}
          			
          			if (rank.equalsIgnoreCase("")) {
              			
              			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Rank: " + ChatColor.RED + "None");		
              			
              		} else {
              			
              			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Rank: " + rank);
              			
              		}
          		
          	 		if (Main.mysql.isVanished(target.getName())) {
              			
              			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Hidden: " + ChatColor.GREEN + "TRUE");
              		
              		} else {
              			
              			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Hidden: "  + ChatColor.RED + "FALSE");
              			
              		}
              		
          			
          			if (!target.isOnline()) {	
          		
          			return true;
          			
          			}
          		
      
          		sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "Gamemode: " + ChatColor.RED + target.getPlayer().getGameMode().toString());
          		
          		if (Main.viewinvs.contains(target.getName().toLowerCase())) {
          			
          			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "View Inventories: " + ChatColor.GREEN + "TRUE");
          		
          		} else {
          			
          			sender.sendMessage(ChatColor.RED + target.getName() + "> " + ChatColor.GRAY + "View Inventories: "  + ChatColor.RED + "FALSE");
          			
          		}

          		
          		return true;
          		      
  	    	  }
  	    	  
  	    	  else {
  	    	  
  	    		  sender.sendMessage(ChatColor.RED + "PlayerInfo> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_AQUA + ChatColor.BOLD + "HELPER" + ChatColor.RESET + ChatColor.GRAY + " or higher to run this command!");
      	    	  return true;	  
  	    		     	    	  
  	    	  }
  	    	  
  	    	
  	      
  	      
  	      } 
  	      
  	      else {
  	    	 
  	    	  sender.sendMessage(ChatColor.RED + "PlayerInfo> " + ChatColor.GRAY + "Only players can use this command!");
  	    	  return true;  
  	    	  
  	      }
      		
  		
  		}

		return true;
		
	}
	}
