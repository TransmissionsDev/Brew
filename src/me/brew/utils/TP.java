package me.brew.utils;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class TP implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		  if (cmd.getName().equalsIgnoreCase("tp")) {
	        	 if (sender instanceof Player) {   		 
	        		 if (Main.mysql.modOrHigher(sender.getName())) {

	        			Player player = (Player) sender;
	        			
	        			if (args.length == 0) {
	        		
	        				sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + "You did not specify a player to teleport to! EG: " + ChatColor.RED + "/tp [Target Player] <Destination Player>");
	        				return true;
	        		
	        			} 
	        	
	        	
	        			if (args.length == 1) {
	        		
	        		
	        				Player target = Main.getPlugin().getServer().getPlayer(args[0]);
	        		
	        				if (target == null) {
	        			
	        					sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + args[0] + " is not an online player! EG: " + ChatColor.RED + "/tp [Target Player] <Destination Player>");           	
	        					return true;
	        					
	        				} 
	        		
	        		
	        				player.teleport(target.getLocation());
	        				sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + "You teleported to " + ChatColor.RED + target.getPlayerListName());
	        		
	        				for (Player p : Bukkit.getOnlinePlayers()) {
	           		  
	        					if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	        					
	        						ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has teleported to " + target.getPlayerListName()  );
	        					
	        					} 
	        				}
	              
	        				return true;
		
	        	}
	        	
	        	if (args.length > 1) {
	        		
	        		Player target = Main.getPlugin().getServer().getPlayer(args[0]);
	        		Player target2 = Main.getPlugin().getServer().getPlayer(args[1]);
	        		
	        		if (target == null) {
	        			
	        			sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + args[0] + " is not an online player! EG: " + ChatColor.RED + "/tp [Target Player] <Destination Player>");
	                 	
	        			return true;
	        		} 
	        		
	        		
	        		if (target2 == null) {
	        			
	        			sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + args[1] + " is not an online player! EG: " + ChatColor.RED + "/tp [Target Player] <Destination Player>");
	                 	
	        			return true;
	        		} 
	        		
	        		
	        		target.teleport(target2.getLocation());
	        		
	        		for (Player p : Bukkit.getOnlinePlayers()) {  
	        			if (Main.mysql.adminOrHigher(p.getName())) {
	             			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	              			
	             				 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has teleported " + target.getPlayerListName() + ChatColor.GRAY + " to " + target2.getPlayerListName()  );
	             			
	             			 }
	              		 }
	                
	             		  
	               
	                }    
	        		
	        		sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + "You teleported " + ChatColor.RED + target.getPlayerListName() + ChatColor.GRAY + " to "  + ChatColor.RED + target2.getPlayerListName());
	        		return true;
	        		
	        		
	            	}
	        	
	   			  	} 
	        		
	        		else {
	        		  
	    				 sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
	                 	 return true;
	                 	 
	        		}
	        	  
	        	 	} else {
	        		  
	        		  sender.sendMessage(ChatColor.RED + "Tp> " + ChatColor.GRAY + "Only players can use this command!");
	        		  return true;
	        	 	}
	        	 
	        	 
	        	 }

		return true;
		
	}
	}
