package me.brew.utils;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class Gamemode implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	  
		   if (cmd.getName().equalsIgnoreCase("gamemode")) {
	        	  if (sender instanceof Player) {
	    			  if (Main.mysql.adminOrHigher(sender.getName())) {
	        	
	    				  Player player = (Player) sender;
	        	 
	    				  if (args.length == 0) {
	    					  if (player.getGameMode() == GameMode.SURVIVAL) {
	    						  
	    						  player.setGameMode(GameMode.CREATIVE);
	    						  player.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to CREATIVE.");
	            	
	    						  for (Player p : Bukkit.getOnlinePlayers()) {
	                  	
	            	       
	            		  
	    							  if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	    								
	  
	    									  ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set their gamemode to: " + ChatColor.RED + player.getGameMode().toString());
	    								
	    								  
	    							  }
	               
	            		  
	              
	    						  }    
	                  
	    						  return true;
	    						  
	    					  } 
	                
	    					  if (player.getGameMode() == GameMode.CREATIVE) {
	    						  
	    						  player.setGameMode(GameMode.SURVIVAL);
	    						  player.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SURVIVAL.");
	            	 
	    						  for (Player p : Bukkit.getOnlinePlayers()) {         		  
	    							  if (Main.mysql.adminOrHigher(p.getName())) {
	    								  if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	    									 
	    									  ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set their gamemode to: " + ChatColor.RED + player.getGameMode().toString());
	    								  
	    								  }
	    							  }
	    						  }    
	                  
	    						  return true;
	                  
	    					  }
	                
	    					  if (player.getGameMode() == GameMode.SPECTATOR) {
	    						  player.setGameMode(GameMode.SURVIVAL);
	    						  player.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SURVIVAL.");
	                    
	    						  for (Player p : Bukkit.getOnlinePlayers()) {
	    							  if (Main.mysql.adminOrHigher(p.getName())) {
	    								  if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	    							
	    									  ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set their gamemode to: " + ChatColor.RED + player.getGameMode().toString());
	    								  
	    								  }
	    							  }
	    						  }	    
	                    
	    						  return true;
	                    
	                    
	    					  }
	                
	                
	    				  }
	            
	               
	    				  
	    			Player target = org.bukkit.Bukkit.getPlayer(args[0]);
	               
	                
	                if (target == null) {
	                	if (args[0].equalsIgnoreCase("creative")) {
	                	 
	                	 player.setGameMode(GameMode.CREATIVE);
	                	 sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to CREATIVE!");
	                	
	              
	                	 for (Player p : Bukkit.getOnlinePlayers()) {
	                		 if (Main.mysql.adminOrHigher(p.getName())) {
	                			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                				 
	                				 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set their gamemode to: " + ChatColor.RED + player.getGameMode().toString());
	            			  }
	             		 }
	               
	            		  
	              
	                	 }    
	                	 
	                	 return true;
	                	
	                 }
	                 
	                 if (args[0].equalsIgnoreCase("survival")) {
	                	 
	                	 player.setGameMode(GameMode.SURVIVAL);
	                	 sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SURVIVAL!");
	                	
	                	 for (Player p : Bukkit.getOnlinePlayers()) {
	                		 if (Main.mysql.adminOrHigher(p.getName())) {
	                			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	            				  
	            				  ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set their gamemode to: " + ChatColor.RED + player.getGameMode().toString());
	            			 
	                			 }
	                		 }
	               
	            		  
	              
	                	 }    
	                	 
	                	 return true;
	                	  
	                 }
	                 
	                 if (args[0].equalsIgnoreCase("spectator")) {
	                	
	                	 player.setGameMode(GameMode.SPECTATOR);
	                	 sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SPECTATOR!");
	                	
	                	 for (Player p : Bukkit.getOnlinePlayers()) { 
	                		 if (Main.mysql.adminOrHigher(p.getName())) {
	                			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                				 
	                				 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set their gamemode to: " + ChatColor.RED + player.getGameMode().toString());
	            			 
	                			 }
	                		 }
	                	 }    
	                	 
	                	 return true;
	                	 
	                 }
	               
	                 
	                 sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You did not specify a valid player or gamemode! EG: " + ChatColor.RED + "/gamemode [Player] <Creative | Survival | Spectator>");
	             	return true;
	                 
	                 
	                } 
	                
	             
	            
	               if (args.length > 1) {	
	                
	            	   if (args[1].equalsIgnoreCase("creative")) {
	               	 
	            		   target.setGameMode(GameMode.CREATIVE);
	               	 
	            		   sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You have set " + target.getName() + "'s gamemode to CREATIVE.");
	            		   target.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to CREATIVE.");
	          
	            		   for (Player p : Bukkit.getOnlinePlayers()) {
	            			   if (Main.mysql.adminOrHigher(p.getName())) {
	            				   if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	            					   ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set " + target.getPlayerListName() + ChatColor.GRAY + "'s gamemode to: " + ChatColor.RED + target.getGameMode().toString());
	            				   }
	            			   }
	            		   }    
	                
	            		   return true;
	            	   }
	                
	            	   if (args[1].equalsIgnoreCase("survival")) {
	               	 
	                  	 target.setGameMode(GameMode.SURVIVAL);
	                  	 
	                  	 sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You have set " + target.getName() + "'s gamemode to SURVIVAL.");
	                  	 target.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SURVIVAL.");
	             
	                  	 for (Player p : Bukkit.getOnlinePlayers()) {
	                  		 if (Main.mysql.adminOrHigher(p.getName())) {
	                  			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                  				 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set " + target.getPlayerListName() + ChatColor.GRAY + "'s gamemode to: " + ChatColor.RED + target.getGameMode().toString());
	                  			 }
	                  		 }
	                     }    
	                   
	                   return true;

	                }
	                
	                if (args[1].equalsIgnoreCase("spectator")) {
	               	 
	                  target.setGameMode(GameMode.SPECTATOR);
	                  	 
	                  sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You have set " + target.getName() + "'s gamemode to SPECTATOR.");
	                  target.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SPECTATOR.");
	               
	                   for (Player p : Bukkit.getOnlinePlayers()) {
	               		  if (Main.mysql.adminOrHigher(p.getName())) {
	               			  if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                			 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set " + target.getPlayerListName() + ChatColor.GRAY + "'s gamemode to: " + ChatColor.RED + target.getGameMode().toString());
	               			  } 
	               		  }
	                   }    
	                   
	                   return true;
	                  
	                }
	                
	                
	                sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You did not specify a valid gamemode! EG: " + ChatColor.RED + "/gamemode [Player] <Creative | Survival | Spectator>");
	             	return true;
	                
	               }
	               
	               
	               if (target.getGameMode() == GameMode.SURVIVAL) {
	                  
	            	   target.setGameMode(GameMode.CREATIVE);
	                  
	                   sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You have set " + target.getName() + "'s gamemode to CREATIVE.");
	                   target.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to CREATIVE.");
	              
	                   for (Player p : Bukkit.getOnlinePlayers()) { 
	               		  if (Main.mysql.adminOrHigher(p.getName())) {
	               			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {	 
	                			 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set " + target.getPlayerListName() + ChatColor.GRAY + "'s gamemode to: " + ChatColor.RED + target.getGameMode().toString());
	               			 } 
	               		  }
	                   }    
	                   
	                   return true;
	               
	               } 
	                 
	                 if (target.getGameMode() == GameMode.CREATIVE) {
	                	 
	                	 target.setGameMode(GameMode.SURVIVAL);
	                     
	                     sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You have set " + target.getName() + "'s gamemode to SURVIVAL.");
	                     target.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SURVIVAL.");
	                    
	                     for (Player p : Bukkit.getOnlinePlayers()) {
	                    	 if (Main.mysql.adminOrHigher(p.getName())) {
	                 			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                 				 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set " + target.getPlayerListName() + ChatColor.GRAY + "'s gamemode to: " + ChatColor.RED + target.getGameMode().toString());
	                 			 }
	                  		 }
	                     }    
	                     
	                     return true;
	                     
	                 }
	                 
	                 if (target.getGameMode() == GameMode.SPECTATOR) {
	                	 target.setGameMode(GameMode.SURVIVAL);
	                     
	                     sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You have set " + target.getName() + "'s gamemode to SURVIVAL.");
	                     target.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Your gamemode has been set to SURVIVAL.");
	                   
	                     for (Player p : Bukkit.getOnlinePlayers()) {
	                    	 if (Main.mysql.adminOrHigher(p.getName())) {
	                 			 if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                  			 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has set " + target.getPlayerListName() + ChatColor.GRAY + "'s gamemode to: " + ChatColor.RED + target.getGameMode().toString());
	                 			 }
	                  		 }
	                     }    
	                     
	                     return true;
	                     
	                     
	                   }

	    		  }
	    			  
	    		  else {
	        		  
	    				 sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
	                 	 return true;
	        	  }
	        	  
	        	  }
	        	  
	        	  else {
	        		  
	        		  sender.sendMessage(ChatColor.RED + "Gamemode> " + ChatColor.GRAY + "Only players can use this command!");
	        		  return true;
	        	  }
	        	  
	        }
		
		return true;
		
	}
	}
