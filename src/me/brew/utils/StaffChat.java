package me.brew.utils;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;

import org.bukkit.command.CommandExecutor;

public class StaffChat implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	       if (cmd.getName().equalsIgnoreCase("staffchat")) {
	       	  if (sender instanceof Player) {
	         	if (Main.mysql.isStaff(sender.getName())) {
	         	   if (args.length > 0) {

	         		   ArrayList < String > al = new ArrayList < String > ();

	                    for (String p: args) {
	                        al.add(p);


	                    }


	          

	                    String reason = "";

	                    for (String p: al) {

	                        reason = reason + " " + p;

	                    }

	                  
	                    Player player = (Player) sender;
	         	
	         	
	                    for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
	         		
	                 	   if (Main.mysql.isStaff(p.getName())) {
	         		
	         	
	                 		   p.sendMessage(ChatColor.GOLD + "Staff> " + player.getPlayerListName()  + ChatColor.GRAY + reason);
	         		
	                 	   }
	                    }
	         	
	         } else {
	         	
	         	sender.sendMessage(ChatColor.GOLD + "Staff> " + ChatColor.GRAY + "You must provide a message to send to all staff!");
	         	return true;
	         }
	         	   
	         } else {
	         	
	         	sender.sendMessage(ChatColor.GOLD + "Staff> " + ChatColor.GRAY + "You must be a staff member to run this command!");
	         	return true;
	         }
	       
	         } else {
	         	sender.sendMessage(ChatColor.GOLD + "Staff> " + ChatColor.GRAY + "Only players can use this command!");
	    		 
	         	return true;
	         }
	       	  
	       
	         }
		return true;
		
	}
	}
