package me.brew.assistance;

import java.util.ArrayList;




import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class Assistance implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {


        if (cmd.getName().equalsIgnoreCase("assistance")) {
        	  if (sender instanceof Player) {
          
          	   if (args.length > 0) {
                     ArrayList < String > al = new ArrayList < String > ();

                     for (String p: args) {
                         al.add(p);


                     }


           

                     String reason = "";

                     for (String p: al) {
 
                         reason = reason + " " + p;

                     }

                   
                     Player player = (Player) sender;
          
          	sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + player.getPlayerListName()  + ChatColor.GRAY + ":" + reason);
        
     
          	for (Player p : Bukkit.getServer().getOnlinePlayers()) {
          	if (!(p.getName().equalsIgnoreCase(player.getName())))	 {
          	if (Main.mysql.isStaff(p.getName())) {
          		
          	 
          		p.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + player.getPlayerListName()  + ChatColor.GRAY  + ":" + reason);
          		
          	}
          	}
          	}
          	   }
          	   
          	   
          	
           else {
          	
          	sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + "You must provide a message to send out to staff for assistance!");
          	return true;
          }
          	   
      
        
          } else {
          	sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + "Only players can use this command!");
          	return true;
          }
        }
        	  
        if (cmd.getName().equalsIgnoreCase("assistancereply")) {
      	  if (sender instanceof Player) {
            if (Main.mysql.isStaff(sender.getName())) {
        	   if (args.length > 1) {
                   ArrayList < String > al = new ArrayList < String > ();

           
                   
                   for (String p: args) {
                       al.add(p);


                   }
                
                   al.remove(0);

         

                   String msg = "";

                   for (String p: al) {

                       msg = msg + " " + p;

                   }

                   
        		   Player target = Bukkit.getServer().getPlayer(args[0]);
        		   
        		   if (target == null) {
        			   
        			   sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + "You must specify an online player to reply to and a mesage to send to them! EG: /assistancereply <Player> <Message>");
            		   return true;
        		   }
        		  
        		   sender.sendMessage(ChatColor.RED + "Assistance> " + ((Player) sender).getPlayerListName()  + ChatColor.RED + " -> " + target.getPlayerListName() + ChatColor.GRAY + ":" + msg);
        		   if (sender.getName() != target.getName()) {
        		  
        			 target.sendMessage(ChatColor.RED + "Assistance> " + ((Player) sender).getPlayerListName()  + ChatColor.RED + " -> " + target.getPlayerListName() + ChatColor.GRAY + ":" + msg);
        		
        		   }
        		   
        		   return true;
        	   }
        	   
        	   if (args.length == 1 || args.length == 0) {
        		   
        		   sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + "You must specify an online player to reply to and a mesage to send to them! EG: /assistancereply <Player> <Message>");
        		   return true;
        	   }
        	   
      	  } else {
      		sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + "You must be a staff member to use this command!");
        	return true; 
      		  
      	  }
            
      	  } else {
      		
            	sender.sendMessage(ChatColor.RED + "Assistance> " + ChatColor.GRAY + "Only players can use this command!");
            	return true;
      		  
      	  }
      	  
        }

	   
	    
		
		return true;
	}
	
	

}
