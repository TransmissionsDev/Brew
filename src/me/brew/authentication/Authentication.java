package me.brew.authentication;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;




import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

import me.brew.Main;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.ArrayList;
import org.bukkit.command.CommandExecutor;

public class Authentication implements Listener, CommandExecutor {
    public static ArrayList < String > locked = new ArrayList < String > ();	

	
	

	 	public static Boolean islocked(String uuid) {
			
	 		if (locked.contains(uuid)) {
				return true;	
			}
			
			return false;
	 	}
	 	
	 	
	
	 public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {


		
		if (cmd.getName().equalsIgnoreCase("2fa") &&  Main.getPlugin().getConfig().getBoolean("2fa-module")) {
			if (Main.mysql.hosOrHigher(sender.getName()) || !(sender instanceof Player)) {
			if (args.length == 0) {
				sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Commands: secretkey, reset, code!");
				return true;
			}

			
			if (args[0].equalsIgnoreCase("code")) {
				if (args.length > 1) {
					

					OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);   
					for(String check : Main.auth.keySet()) {
						if(check.equalsIgnoreCase(p.getUniqueId().toString().toLowerCase())) {
							GoogleAuthenticator gAuth = new GoogleAuthenticator();
							sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + p.getName() + "'s current TOTP code: " + gAuth.getTotpPassword(Main.auth.get(p.getUniqueId().toString().toLowerCase())));
							return true;
						}
						
					}
					
				
					
					sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + p.getName() + " does not have a secret key associated with their account, therefore a TOTP code cannot be retrived!");	
					return true;
					
				} else {
					
					sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "You must specify a player to lookup their current TOTP code! EG: /auth code <Player>");	
					return true;	
					
				}
			}
			
			if (args[0].equalsIgnoreCase("reset")) {
				if (args.length > 1) {
					OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);   	
					for(String check : Main.auth.keySet()) {
						if(check.equalsIgnoreCase(p.getUniqueId().toString().toLowerCase())) {
							Main.auth.remove(p.getUniqueId().toString().toLowerCase());
							sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + p.getName() + "'s secret key has been reset!");	
							return true;
						}
						
						
					}
					
					sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + p.getName() + " does not have a secret key associated with their account!");	
					return true;
				
				} else {	
					sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "You must specify a player to reset their secret key! EG: /auth reset <Player>");	
					return true;
				}
				
			}
			
			if (args[0].equalsIgnoreCase("secretkey")) {
				if (args.length > 1) {
					OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);   	
					if(Main.auth.get(p.getUniqueId().toString().toLowerCase()) != null) {
						sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Secret key for " + p.getName() + ": " + Main.auth.get(p.getUniqueId().toString().toLowerCase()));	
					} else {
						
						sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "There is no secret key associated with " + args[1]);
						
					}
				
				return true;
				
				} else {
				sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "You must specify a player to lookup! EG: /auth secretkey <Player>");	
				return true;
				}
				
				
			}
			
			sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Commands: secretkey, reset, code!");
			return true;
			
		} else {
			
			  sender.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_RED + ChatColor.BOLD + "H.O.S." + ChatColor.GRAY + " or higher to run this commmand!");
              return true;	
		}
		
		}
	
		return true;
		
	}

	 	@EventHandler
	    public void chat(AsyncPlayerChatEvent event) {
	 
	        if (locked.contains(event.getPlayer().getUniqueId().toString().toLowerCase())) {
	            Player player = event.getPlayer();
		        String message = event.getMessage();
	        	try {
	        		Integer code = Integer.parseInt(message);
	        		
	        		 String secretkey = Main.auth.get(player.getUniqueId().toString().toLowerCase());

	 	 	        GoogleAuthenticator gAuth = new GoogleAuthenticator();

	 	 	        boolean codeisvalid = gAuth.authorize(secretkey, code);
	 	 	        
	 	 	        if (codeisvalid) {
	 	 	        	
	 	 	        	locked.remove(player.getUniqueId().toString().toLowerCase());
	 	 	        	player.removePotionEffect(PotionEffectType.BLINDNESS);
	 	 	        	player.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Authorized!");
	 	 	        	event.setCancelled(true);
	 	 	        	
	 	 	        } else {
	 	 	        	
	 	 	        	player.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Your code is invalid! Codes will only be numbers and have six digits!");
	 	 	        	event.setCancelled(true);
	 	 	        }
	        	
	        	}
	        	catch (NumberFormatException ex) {
	               
	             event.getPlayer().sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Your code is invalid! Codes will only be numbers and have six digits!");
	             event.setCancelled(true);
	        	}
	        }
	  }
	 

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {  		
	
	locked.remove(e.getPlayer().getUniqueId().toString().toLowerCase());
		
	}
	 	
    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent e) {  
       	Player p = e.getPlayer();
    
       	if (!Main.auth.containsKey(p.getUniqueId().toString().toLowerCase()) && Main.mysql.isStaff(p.getName()) && Main.getPlugin().getConfig().getBoolean("2fa-module")) {
 
    	GoogleAuthenticator gAuth = new GoogleAuthenticator();
		GoogleAuthenticatorKey key = gAuth.createCredentials();
		p.sendMessage("⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯");
		p.sendMessage(ChatColor.BLUE + "§l2FA SETUP INSTRUCTIONS");
		p.sendMessage(ChatColor.BLUE + " ");
		p.sendMessage("As a staff member on this server, you are required to\nsetup 2FA on your account as an extra layer of security.");
		p.sendMessage(ChatColor.BLUE + " ");
		p.sendMessage("Simply input the secret key below into a TOTP app like Google\nAuthenticator and type the 6 digit code you recive into the\nchat. Everytime you join the server you will have to input\na new code given to you by the app. The code will change\nevery " + ChatColor.AQUA + "30 seconds" + ChatColor.WHITE + ".");
		p.sendMessage("⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯");
	
		
		TextComponent message = new TextComponent(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Secret Key: " + key.getKey());
		message.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "" + gAuth.getTotpPassword(key.getKey())));
		message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.BLUE + key.getKey()).create()));
		p.spigot().sendMessage(message);
		
		Main.auth.put(p.getUniqueId().toString().toLowerCase(), key.getKey());
		locked.add(p.getUniqueId().toString().toLowerCase());
		p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 0));
    
   	

    } else {
    	if(Main.mysql.isStaff(p.getName()) && Main.getPlugin().getConfig().getBoolean("2fa-module")) {
    	locked.add(e.getPlayer().getUniqueId().toString().toLowerCase());
    	
    	p.sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Please enter your auth code!");
    	p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 0));
    	
    	}
    
    }
	

	}

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent event) {
    	 if (locked.contains(event.getPlayer().getUniqueId().toString().toLowerCase())) {
    		event.getPlayer().sendMessage(ChatColor.BLUE + "2fa> " + ChatColor.GRAY + "Please enter your auth code!");
    		event.setCancelled(true); 
    	 }
    }

    @EventHandler
    public void move(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (locked.contains(player.getUniqueId().toString().toLowerCase())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void blockbreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (locked.contains(player.getUniqueId().toString().toLowerCase())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void blockplace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (locked.contains(player.getUniqueId().toString().toLowerCase())) {
            event.setCancelled(true);
        }
}
    
}
