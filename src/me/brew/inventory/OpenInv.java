package me.brew.inventory;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.CommandExecutor;

public class OpenInv implements Listener, CommandExecutor  {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	
		if (cmd.getName().equalsIgnoreCase("openinv")) { 
    		 if (sender instanceof Player) {	
    		 if (Main.mysql.adminOrHigher(sender.getName()) ) {
    			 
    				if (args.length == 0) {
    	  			  sender.sendMessage(ChatColor.RED + "OpenInventory> " + ChatColor.GRAY + "You must specify a player to open their inventory! Ex: " + ChatColor.RED + "/openinv <User>"); 
    	 
    	  			  return true;
    	  			 
    	  		 } else {
    	  			 Player player = (Player) sender;
    	  			 
    	  			 Player p = Main.getPlugin().getServer().getPlayer(args[0]);
    	  			
    	  			 if (p == null) {
    	  				 
    	  				 sender.sendMessage(ChatColor.RED + "OpenInventory> " + ChatColor.GRAY + args[0] + " is not a valid player!");
    	  				 return true;
    	  			
    	  			 }
    	  			 
    	  			 
    	  			 if (p.getName().equalsIgnoreCase(player.getName())) {
    	  				 
    	  				sender.sendMessage(ChatColor.RED + "OpenInventory> " + ChatColor.GRAY + "You can't open your own inventory!");
   	  				 	return true;
    	  				 
    	  			 }
    	  			 
    	  			 player.openInventory(p.getInventory());
    	  			
    	  			 sender.sendMessage(ChatColor.RED + "OpenInventory> " + ChatColor.GRAY + "You opened " + p.getPlayerListName() + ChatColor.GRAY + "'s inventory!");
	  				 
    	  			 return true;
    	  			 
    	  		 }
    			
    			  } else {
    				
    				  sender.sendMessage(ChatColor.RED + "OpenInventory> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
                     return true;	
       				
    			  }
    				
    			} else {
    			
    			
                 	  sender.sendMessage(ChatColor.RED + "OpenInventory> " + ChatColor.GRAY + "Only players can use this commmand!");
                   	 return true;	  
     				  
    				
    			}
    			}
		 
		return true;
		
	}
	}
