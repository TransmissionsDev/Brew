package me.brew.inventory;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class ViewInvs implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
    	
		if (cmd.getName().equalsIgnoreCase("viewinvs")) { 
      		 if (sender instanceof Player) {	
      		 if (Main.mysql.adminOrHigher(sender.getName())) {
      			 
      			 Player player = (Player) sender;
      			 
      			 if (args.length == 0) {
      				 
      				 if (Main.viewinvs.contains(sender.getName().toLowerCase())) {
      					 
      					Main.viewinvs.remove(sender.getName().toLowerCase());
      				
      				
      					
      				  for (Player p : Bukkit.getOnlinePlayers()) {
                       	
                         
      	          		  
      					 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
      						 
      	           			 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has disabled view inventories" );
      	          			} 
      	           		 }
      	             
      	          		  
      	            
      	                   
      					
      					 sender.sendMessage(ChatColor.RED + "ViewInvs> " + ChatColor.GRAY + "You have disabled your ability to open other player's inventories!");
      					 return true;
      				 } else {
      					Main.viewinvs.add(sender.getName().toLowerCase());
      					 
      			
        				
      					
      					for (Player p : Bukkit.getOnlinePlayers()) {
                       	
                         
      	          		  
      	          		  if (Main.mysql.adminOrHigher(p.getName())) {
      	           			if (Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
      	           			 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has enabled view inventories");
      	           			}
      	           		 }
      	             
      	          		  
      	            
      	                }    
      					
      					 sender.sendMessage(ChatColor.RED + "ViewInvs> " + ChatColor.GRAY + "You have enabled the ability to view other player's inventories!");
      					 return true;
      					 
      				 }
      				 
      			 } else {
      				 
      				 Player target = Main.getPlugin().getServer().getPlayer(args[0]);
      				 
      				 if (target == null) {
      					 
      					sender.sendMessage(ChatColor.RED + "ViewInvs> " + ChatColor.GRAY + args[0] + " is not an online player!");
     					 return true;
     					 
      					 
      				 }
      				 
      				 
      				 if (Main.viewinvs.contains(target.getName().toLowerCase())) {
      					 
       					Main.viewinvs.remove(target.getName().toLowerCase());
       					 
       				  for (Player p : Bukkit.getOnlinePlayers()) {
                        
       	          		  if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
       	          		
       	           			
       	          				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has disabled view inventories for " + target.getPlayerListName()  );
       	          			
       	          			
       	           		 }
       	             
       	          		  
       	            
       	                }    
       					
       					 sender.sendMessage(ChatColor.RED + "ViewInvs> " + ChatColor.GRAY + "You have disabled " + target.getPlayerListName() + ChatColor.GRAY + "'s ability to view other player's inventories!");
       					 return true;
       				 } else {
       					Main.viewinvs.add(target.getName().toLowerCase());
       					 
       					for (Player p : Bukkit.getOnlinePlayers()) {
                        	
                          
       	          		  
       	          		  if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
       	          	
       	          			 ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has enabled view inventories for " + target.getPlayerListName()  );
       	          			
       	           		 }
       	             
       					}
       					
       					 sender.sendMessage(ChatColor.RED + "ViewInvs> " + ChatColor.GRAY + "You have enabled " + target.getPlayerListName() + ChatColor.GRAY + "'s ability to view other player's inventories!");
       					 return true;
       					
       				 }
      				 
      				 
      			 }
      			 
      			 
      			 
      			 
      		 } else {
   	   		  sender.sendMessage(ChatColor.RED + "Inventory> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
   	          return true; 
      			 
      		 }
      		 } else {
      			 sender.sendMessage(ChatColor.RED + "Inventory> " + ChatColor.GRAY + "Only players can use this command!");
                return true;  
      			 
      		 }
       		
       	}
		return true;
		
	}
	}
