package me.brew.database;

import java.util.ArrayList;





import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.brew.Board;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandExecutor;

public class Prefix implements CommandExecutor {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

if (cmd.getName().equalsIgnoreCase("prefix")) {
	if (sender instanceof Player) {
		if (Main.mysql.hosOrHigher(sender.getName())) {

			if (args.length == 0 || args.length == 1) {
			
		    sender.sendMessage(ChatColor.RED + "Prefix> " + ChatColor.GRAY + "You must specify a player and the prefix you want to be applied! EG: /prefix <Player> <Prefix>");
		    return true;
		    
			} else {

		        ArrayList < String > list = new ArrayList < String > ();

	
                for (String p: args) {
                    list.add(p);
                    list.add(" ");


                }
               
                list.remove(0);
                list.remove(0);
                list.remove(list.size() - 1);

      

                String prefix = "";

                for (String p : list) {
                	
                    prefix = prefix + p;

                }
             
              	
				
			if (Main.mysql.getPrefix(args[0].toLowerCase()).equalsIgnoreCase("")) {
			
				Main.mysql.addPrefix(args[0].toLowerCase(), prefix);
				
				
			} else {
				
				Main.mysql.updatePrefix(args[0].toLowerCase(), prefix);
			
				
			}
			
			Player target = Main.getPlugin().getServer().getPlayer(args[0]);
			
			if (target != null) {
				  
  			  com.nametagedit.plugin.NametagEdit.getApi().setNametag(target, Main.mysql.getPrefix(target.getName()), "");
			  
  			  target.setPlayerListName("§7" + Main.mysql.getPrefix(target.getName()) + target.getName());
  		
  			  target.setScoreboard(Board.getdefaultboard(target.getName()));
  			 
			  sender.sendMessage(ChatColor.RED + "Prefix> " + ChatColor.GRAY + args[0] + "'s prefix has been set to: " + ChatColor.RED + Main.mysql.getPrefix(args[0])); 
			  return true;
				
			}

    		if (sender instanceof Player) {
    	    ByteArrayDataOutput out = ByteStreams.newDataOutput();
        	out.writeUTF("KickPlayer");
        	out.writeUTF(args[0]);
        	out.writeUTF(ChatColor.RED + "Your rank has been updated from another bungee instance! As a safety measure you have been kicked from the server.\n" + ChatColor.GRAY + "Please reconnect.");
        	Player player = (Player) sender;
        	
        	Main.sendPluginMsg(player, out);

    		  
      	  sender.sendMessage(ChatColor.RED + "Prefix> " + ChatColor.GRAY + args[0] + "'s prefix has been set to: " + ChatColor.RED + Main.mysql.getPrefix(args[0])); 
		  return true;
    	
    		} else {
    
    		  sender.sendMessage(ChatColor.RED + "\nPrefix> " + ChatColor.GRAY + "UH OH! You're using the console, therefore BYPASSING the kick SAFETY measure put in place to solve nametag and scoreboard issues. Please note that this may cause issues with player scoreboards and nametags! \n");
    		  return true; 
	       	}
	  		
		
	
			 
			}
		} else {

			sender.sendMessage(ChatColor.RED + "Prefix> " + ChatColor.GRAY + "You must be " + ChatColor.DARK_RED + ChatColor.BOLD + "H.O.S." + ChatColor.GRAY + " or higher to run this commmand!");
			return true;

		}

	} else {

		sender.sendMessage(ChatColor.RED + "Prefix> " + ChatColor.GRAY + "Only players can use this command!");
		return true;

	}

}
		
		return true;
	}
	

}
