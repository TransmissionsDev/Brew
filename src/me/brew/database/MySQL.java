
package me.brew.database;

import java.sql.Connection;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {
	
    private Connection connection;

    private String storeip;
    private String storeuser;
    private String storepass;
    private String storedb;
    

    
    
    public MySQL(String ip, String userName, String password, String db) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + ip + "/" + db + "?user=" + userName + "&password=" + password);
            storeip = ip;
            storeuser = userName;
            storepass = password;
            storedb = db;
        } catch (Exception e) {
            e.printStackTrace();
           
        }
    }
    
 
        
    public String getBanReason(String user)  {
    	try {
    		
    		
    		 
    		PreparedStatement statement = connection.prepareStatement("select reason from bans where name=(?)");
    		statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
    		if (result.next()) {
    			return result.getString("reason");
    		} else {
    			return "";
    		}
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
		
    		PreparedStatement statement = connection.prepareStatement("select reason from bans where name=(?)");
    		statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
    		if (result.next()) {
    			return result.getString("reason");
    		} else {
    			return "";
    		}
    	
    	} catch (SQLException e2) {
    		e.printStackTrace();
    		return null;
    	}
    }
		
    }
    
    
    
    public String getBanType(String user)  {
    	try {
    		  
     	
    		
    		PreparedStatement statement = connection.prepareStatement("select bantype from bans where name=(?)" );
    	  statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
            if (result.next()) {
               
            
            	return result.getString("bantype");
       
            
            } else {
                return null;
        }
    		
  
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
				
    		PreparedStatement statement = connection.prepareStatement("select bantype from bans where name=(?)" );
      	  statement.setString(1, user.toLowerCase());
      		
      		ResultSet result = statement.executeQuery();
      		
              if (result.next()) {
                 
              
              	return result.getString("bantype");
         
              
              } else {
                  return null;
              }
      		
    	
    	
    	} catch (SQLException e2) {
        	e2.printStackTrace();
        	return null;  
       }
    		
    }
		
    }
    
    
    public String getBanner(String user)  {
    	try {
    		 
    		PreparedStatement statement = connection.prepareStatement("select banner from bans where name=(?)");
    		statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
    		if (result.next()) {
    			return result.getString("banner");
    		} else {
    			return "";
    		}
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
    		PreparedStatement statement = connection.prepareStatement("select banner from bans where name=(?)");
    		statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
    		if (result.next()) {
    			return result.getString("banner");
    		} else {
    			return "";
    		}
    		} catch (SQLException e2) {
    			e.printStackTrace();
    			return null;
    		}
    	}
    }       
    
    
    public Long getBanDuration(String user)  {
    	try {
    		PreparedStatement statement = connection.prepareStatement("select duration from bans where name=(?)");
    		statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
    		if (result.next()) {
    			return result.getLong("duration");
    		} else {
    			return null;
    		}
    	} catch (Exception e) {
    		try {
				connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
			
    		
    		PreparedStatement statement = connection.prepareStatement("select duration from bans where name=(?)");
    		statement.setString(1, user.toLowerCase());
    		
    		ResultSet result = statement.executeQuery();
    		
    		
    		if (result.next()) {
    			return result.getLong("duration");
    		} else {
    			return null;
    		}
    		
    		}
    		
    		catch (SQLException e1) {
				
				e1.printStackTrace();
				return null;
			}
    	}
    }       
    
    
  
    
    public void banPlayer(String banner, String player, String reason)  {
    	try {
    	
    		
    		
    		PreparedStatement statement = connection.prepareStatement("insert into bans (name, reason, banner, bantype)\nvalues (?, ?, ?, ?);");
    	statement.setString(1, player.toLowerCase());
       	statement.setString(2, reason);
       	statement.setString(3, banner);
    	statement.setString(4, "perm");
    	
    		
    		statement.executeUpdate();
    		statement.close();
    	} catch (Exception e) {
        	try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
    		PreparedStatement statement = connection.prepareStatement("insert into bans (name, reason, banner, bantype)\nvalues (?, ?, ?, ?);");
    	statement.setString(1, player.toLowerCase());
       	statement.setString(2, reason);
		statement.setString(3, banner);
		
    	statement.setString(4, "perm");
    	
    		
    		statement.executeUpdate();
    		statement.close();
    		
        	} catch (SQLException e1) {
    			e1.printStackTrace();
    		}
        	
    	}
    }
    
 
    public String getRank(String player)  {
    	try { 
    	
    		 	
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
		statement.setString(1, player.toLowerCase());
		
		ResultSet result = statement.executeQuery();
	
		if (result.next()) {
			return result.getString("rank");
		} else {
			return "";
		}
    	}
    	
  	catch (Exception e) {
    	
  		try {
    		
    			connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    			PreparedStatement statement = connection.prepareStatement("select prefix from ranks where username=(?)");
				statement.setString(1, player.toLowerCase());
				
				ResultSet result = statement.executeQuery();
		
			if (result.next()) {
				return result.getString("rank");
			} else {
				return "";
			}
	    	
  		} catch (SQLException e2){
  			
  			e2.printStackTrace();
  			return null;
  			
  		}
	    	
			
    		
    		
    	    
    	
	
	}
		

	} 
    
    public String getPrefix(String player)  {
    	try {
    	
    		 	
    		PreparedStatement statement = connection.prepareStatement("select prefix from ranks where username=(?)");
		statement.setString(1, player.toLowerCase());
		
		ResultSet result = statement.executeQuery();
	
		if (result.next()) {
			return result.getString("prefix");
		} else {
			return "";
		}

	} 
    	
    	catch (Exception e) {
    		
    		try {
    			connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    			PreparedStatement statement = connection.prepareStatement("select prefix from ranks where username=(?)");
				statement.setString(1, player.toLowerCase());
				
				ResultSet result = statement.executeQuery();
				e.printStackTrace();
				if (result.next()) {
					return result.getString("prefix");
				} else {
					return "";
				}
    		} catch(SQLException e2) {
    			e2.printStackTrace();
    			return null;
    			
    		}
			
    		
    		
    	    
    		
	
	}
		
		
    }

    public Boolean isVanished(String player)  {
    	try {
    	
    		 	
    		PreparedStatement statement = connection.prepareStatement("select id from vanished where user=(?)");
		statement.setString(1, player.toLowerCase());
		
		ResultSet result = statement.executeQuery();
	
		if (result.next()) {
			return true;
		} else {
			return false;
		}

	} 
    	
    	catch (Exception e) {
    		
    		try {
    			
    			PreparedStatement statement = connection.prepareStatement("select id from vanished where user=(?)");
    			statement.setString(1, player.toLowerCase());
    			
    			ResultSet result = statement.executeQuery();
    		
    			if (result.next()) {
    				return true;
    			} else {
    				return false;
    			}
    			
    		} catch (SQLException e2) {
    			
    			e2.printStackTrace();
    			return null;
    			
    			
    		}
    			
    		 
    
    	    
    		
	
	}
		
		
		
    }

    
    public void unvanish(String player)  {
    	try {
    	
    		 	
    		PreparedStatement statement = connection.prepareStatement("delete from vanished where user=(?);");
    		statement.setString(1, player.toLowerCase());
    		statement.executeQuery();
    		statement.close();
    		
	
	} 
    	
    	catch (Exception e) {
    		
    			try {
    			connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
        		
    			PreparedStatement statement = connection.prepareStatement("delete from vanished where user=(?);");
    			statement.setString(1, player.toLowerCase());
    	   		statement.executeUpdate();
        		statement.close();
    			} catch (SQLException e2) {
    				
    				e2.printStackTrace();
    				
    			}
    	
	
	}
		
		
		
    }

    
    public void vanish(String player)  {
    	try {
    	
    		 	
    		PreparedStatement statement = connection.prepareStatement("insert into vanished (user)\nvalues (?);");
        	statement.setString(1, player.toLowerCase());
    		statement.executeUpdate();
    		statement.close();
	} 
    	
    	catch (Exception e) {
    		
    			try {
    			connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
   
        		PreparedStatement statement = connection.prepareStatement("insert into vanished (user)\nvalues (?);");
    			statement.setString(1, player.toLowerCase());
    			statement.executeUpdate();
    			statement.close();
    			} catch (SQLException e2) {
    				
    				e2.printStackTrace();
    				
    				
    			}
    		
    	    
    		
	
	}
		
		
		
    }

    
    
    public void unbanPlayer(String player)  {
    	try {
    		
    		
    		PreparedStatement statement = connection.prepareStatement("delete from bans where name=(?)");
    		statement.setString(1, player.toLowerCase());
    		statement.executeUpdate();
    		statement.close();
    		
    	
    	
    	} 
    	
    	catch (Exception e) {
    	try {
    	connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
			
    	PreparedStatement statement = connection.prepareStatement("delete from bans where name=(?)");
		statement.setString(1, player.toLowerCase());
		statement.executeUpdate();
		statement.close();
		
    	} catch (SQLException e2) {
    		
    	e2.printStackTrace();	
    		
    	}
	}
    	
    	
    }
    
    
    public String returnRankPrefix(String rank) {

    	
    		if (rank.equalsIgnoreCase("helper")) {
    		 
    		return "§3§lHELPER §3";
   		 
   	 		}
    	
     	
    	    if (rank.equalsIgnoreCase("dev")) {
      		 
    		return "§4§lDEV §c";
   		 
   	 		}
        
   	 		if (rank.equalsIgnoreCase("admin")) {
           	
           	return "§4§lADMIN §c";
           	
           	}
           	
        	if (rank.equalsIgnoreCase("qa")) {
               	
         	return "§6§lQ.A. §6";
               	
            }
        	
        	if (rank.equalsIgnoreCase("hos")) {
               	
           	return "§4§lH.O.S. §c";
               	
          	}


        	if (rank.equalsIgnoreCase("mod")) {
       	
        		return "§2§lMOD §2";
        	}
       
   	
        	if (rank.equalsIgnoreCase("owner")) {
       	
        		return "§4§lOWNER §c";
        	}
        	
        
    
        	return "";
    		
    		}
    
    
    
    public void addRank(String player, String rank) {
    	try {
    		

    	PreparedStatement statement = connection.prepareStatement("insert into ranks (username, rank, prefix)\nvalues (?, ?, ?);");
    	statement.setString(1, player.toLowerCase());
       	statement.setString(2, rank.toLowerCase());
     	
     	statement.setString(3, returnRankPrefix(rank));		
		statement.executeUpdate();
		statement.close();
		
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
    		PreparedStatement statement = connection.prepareStatement("insert into ranks (username, rank, prefix)\nvalues (?, ?, ?);");
        	statement.setString(1, player.toLowerCase());
           	statement.setString(2, rank.toLowerCase());
         	
         	statement.setString(3, returnRankPrefix(rank));		
    		statement.executeUpdate();
    		statement.close();
    		} catch (Exception e2) {
    			
    			e2.printStackTrace();
    			
    		}
    		
    	}
    	
    }
    
    
    public void updateRank(String player, String rank) {
    	try {
    		

    	PreparedStatement statement = connection.prepareStatement("update ranks set rank=(?), prefix=(?) where username=(?);");
       	statement.setString(1, rank.toLowerCase());
       	statement.setString(2, returnRankPrefix(rank.toLowerCase()));	
    	statement.setString(3, player.toLowerCase());
		statement.executeUpdate();
		statement.close();
		
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
    		PreparedStatement statement = connection.prepareStatement("update ranks set rank=(?), prefix=(?) where username=(?);");
           	statement.setString(1, rank.toLowerCase());
           	statement.setString(2, returnRankPrefix(rank.toLowerCase()));	
        	statement.setString(3, player.toLowerCase());
    		statement.executeUpdate();
    		statement.close();
    		} catch (SQLException e2) {
    			
    		e.printStackTrace();
    		
    		}
    	}
    	
    }
    
    public void updatePrefix(String player, String prefix) {
    	try {
    		

    	PreparedStatement statement = connection.prepareStatement("update ranks set prefix=(?) where username=(?);");
       	statement.setString(1, prefix.replaceAll("&", "§"));
    	statement.setString(2, player.toLowerCase());
		statement.executeUpdate();
		statement.close();
		
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    	
    		PreparedStatement statement = connection.prepareStatement("update ranks set prefix=(?) where username=(?);");
           	statement.setString(1, prefix.replaceAll("&", "§"));
        	statement.setString(2, player.toLowerCase());
    		statement.executeUpdate();
    		statement.close();
    		
    		} catch (SQLException e2) {
    			
    			e2.printStackTrace();
    			
    		}
    		
    	}
    	
    }
    
    public void addPrefix(String player, String prefix) {
    	try {
    		

        	PreparedStatement statement = connection.prepareStatement("insert into ranks (username, rank, prefix)\nvalues (?, ?, ?);");
        	statement.setString(1, player.toLowerCase());
         	statement.setString(2, "member");		
        	statement.setString(3, prefix.replaceAll("&", "§"));
       
    		statement.executeUpdate();
    		statement.close();
		
    	} catch (Exception e) {
    		try {
    		
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
        	
    		PreparedStatement statement = connection.prepareStatement("insert into ranks (username, rank, prefix)\nvalues (?, ?, ?);");
        	statement.setString(1, player.toLowerCase());
         	statement.setString(2, "member");		
        	statement.setString(3, prefix.replaceAll("&", "§"));
       
    		statement.executeUpdate();
    		statement.close();
    		} catch (SQLException e2) {
    		
    		e2.printStackTrace();
    		
    		}
    		
    	}
    	
    }

    public boolean adminOrHigher(String player) {
    	try {
    		
    	  	String[] acceptedranks = new String[7];

        	acceptedranks[0] = "owner";
        	acceptedranks[1] = "pman";
        	acceptedranks[2] = "qa";
           	acceptedranks[3] = "hos";
        	acceptedranks[4] = "admin";
        	acceptedranks[5] = "dman";
        	acceptedranks[6] = "dev";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    
    			for (String rank : acceptedranks) {
    		
    	   if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    
		
    	
       	
    	} catch (Exception e) {
    		try {
    		
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    	 	
    		String[] acceptedranks = new String[7];

        	acceptedranks[0] = "owner";
        	acceptedranks[1] = "pman";
        	acceptedranks[2] = "qa";
           	acceptedranks[3] = "hos";
        	acceptedranks[4] = "admin";
        	acceptedranks[5] = "dman";
        	acceptedranks[6] = "dev";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    
    			for (String rank : acceptedranks) {
    		
    	   if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    			
    		return false;
    			
    			
    		} else {
    			return false;
    		}
    		
    		} catch (SQLException e2) {
    		
    		e2.printStackTrace();		
    		return false;
    	
    		}
    		
    	}
		
    	
    }
    
    
    
    public boolean modOrHigher(String player) {
    	try {
    		
    	  	String[] acceptedranks = new String[7];

    		acceptedranks[0] = "admin";
        	acceptedranks[1] = "qa";
           	acceptedranks[2] = "hos";
        	acceptedranks[3] = "owner";
        	acceptedranks[4] = "mod";
          	acceptedranks[5] = "dev";
         	acceptedranks[6] = "owner";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    
		
    	
       	
    	} catch (Exception e) {
    		try {
    	
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    	 	
    	  	String[] acceptedranks = new String[7];

    		acceptedranks[0] = "admin";
        	acceptedranks[1] = "qa";
           	acceptedranks[2] = "hos";
        	acceptedranks[3] = "owner";
        	acceptedranks[4] = "mod";
          	acceptedranks[5] = "dev";
         	acceptedranks[6] = "owner";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    		} catch (SQLException e2) {
		
    	
    		e2.printStackTrace();
    		return false;
    		
    		}
    		
    	}
		
    	
    }

    
    public boolean helperOrHigher(String player) {
    	try {
    		
    	  	String[] acceptedranks = new String[8];

        	acceptedranks[0] = "admin";
        	acceptedranks[1] = "qa";
         	acceptedranks[2] = "hos";
        	acceptedranks[3] = "owner";
        	acceptedranks[4] = "member";
        	acceptedranks[5] = "mod";
        	acceptedranks[6] = "dev";
        	acceptedranks[7] = "helper";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    
		
    	
       	
    	} catch (Exception e) {
    		
    		try {
    		
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
       	 
    		String[] acceptedranks = new String[8];

        	acceptedranks[0] = "admin";
        	acceptedranks[1] = "qa";
         	acceptedranks[2] = "hos";
        	acceptedranks[3] = "owner";
        	acceptedranks[4] = "member";
        	acceptedranks[5] = "mod";
        	acceptedranks[6] = "dev";
        	acceptedranks[7] = "helper";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    
		
    		} catch (SQLException e2) {
    		
    		e2.printStackTrace();
    		
    		return false;
    		
    		}
    		
    	}
		
    	
    }


    
    
    public boolean isStaff(String player) {
    	try {
    	
    	  	String[] acceptedranks = new String[10];
        	acceptedranks[1] = "dman";
    	  	acceptedranks[2] = "admin";
        	acceptedranks[3] = "qa";
           	acceptedranks[4] = "hos";
        	acceptedranks[5] = "owner";
        	acceptedranks[6] = "mod";
        	acceptedranks[7] = "helper";
          	acceptedranks[8] = "dev";
         	acceptedranks[9] = "owner";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    			
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    
		
    	
       	
    	} catch (Exception e) {
    	
    		try {
    		
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);

    	  	String[] acceptedranks = new String[10];
        	acceptedranks[1] = "dman";
    	  	acceptedranks[2] = "admin";
        	acceptedranks[3] = "qa";
           	acceptedranks[4] = "hos";
        	acceptedranks[5] = "owner";
        	acceptedranks[6] = "mod";
        	acceptedranks[7] = "helper";
          	acceptedranks[8] = "dev";
         	acceptedranks[9] = "owner";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    			
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    		} catch (SQLException e2) {
    		
    		e2.printStackTrace();
    		
    		return false;
    		}
    		
    	}
		
    	
    }
    
    
    public boolean hosOrHigher(String player) {
    	try {
    		 
    	  	String[] acceptedranks = new String[3];

           	acceptedranks[0] = "hos";
        	acceptedranks[1] = "owner";
          	acceptedranks[2] = "dev";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}

    		
    
		
    	
       	
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
    	 	String[] acceptedranks = new String[3];

           	acceptedranks[0] = "hos";
        	acceptedranks[1] = "owner";
          	acceptedranks[2] = "dev";
        	

    		
    		PreparedStatement statement = connection.prepareStatement("select rank from ranks where username=(?)");
    		statement.setString(1, player.toLowerCase());
    	
    		ResultSet result = statement.executeQuery();
    		
    		if (result.next()) {
    		for (String rank : acceptedranks) {
    		if (result.getString("rank").equalsIgnoreCase(rank)) {
    			
    			return true;
    		}
    		
    	
    		
    		}
    		
    		return false;
    			
    			
    		} else {
    			return false;
    		}
    		
    		} catch (SQLException e2) {
    		
    		e2.printStackTrace();
    		
    		return false;
    		
    		}
    		
    	}
		
    	
    }
    
    
    
    public void tempbanPlayer(String banner, String player, String reason, long duration) {
    	try {
    	
    		 
    	
    	reason.replaceAll("\'", "\'");
    	
    		 
    		 
    		 
    		
    		PreparedStatement statement = connection.prepareStatement("insert into bans (name, reason, bantype, banner, duration)\nvalues (?, ?, ?, ?, ?);");
    	statement.setString(1, player.toLowerCase());
       	statement.setString(2, reason);
       	statement.setString(3, "temp");
       	statement.setString(4, banner);	
       	statement.setLong(5, duration);

    	
    		
    		statement.executeUpdate();
    		statement.close();
    	} catch (Exception e) {
    		try {
    		connection = DriverManager.getConnection("jdbc:mysql://" + storeip + "/" + storedb + "?user=" + storeuser + "&password=" + storepass);
    		
        	reason.replaceAll("\'", "\'");
        	
        		 
        		 
        		 
        		
        		PreparedStatement statement = connection.prepareStatement("insert into bans (name, reason, bantype, banner, duration)\nvalues (?, ?, ?, ?, ?);");
        	statement.setString(1, player.toLowerCase());
           	statement.setString(2, reason);
           	statement.setString(3, "temp");
           	statement.setString(4, banner);	
           	statement.setLong(5, duration);

        	
        		
        		statement.executeUpdate();
        		statement.close();
    		} catch (SQLException e2) {
    		
    			e2.printStackTrace();
    		
    		}
    	}
    }
    
   

}