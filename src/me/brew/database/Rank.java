package me.brew.database;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.brew.Board;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandExecutor;

public class Rank implements CommandExecutor {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	 	

    	String[] acceptedranks = new String[8];

    	acceptedranks[0] = "admin";
    	acceptedranks[1] = "qa";
     	acceptedranks[2] = "hos";
    	acceptedranks[3] = "owner";
    	acceptedranks[4] = "member";
    	acceptedranks[5] = "mod";
    	acceptedranks[6] = "dev";
    	acceptedranks[7] = "helper";
    	
  	  if (cmd.getName().equalsIgnoreCase("rank")) { 
  		  if (Main.mysql.hosOrHigher(sender.getName()) || !(sender instanceof Player)) {	
  			  if (args.length == 0 || args.length == 1) {
  		  
  			  sender.sendMessage(ChatColor.RED + "Rank> " + ChatColor.GRAY + "You must specify a rank and a player to give it to! Ex: " + ChatColor.RED + "/rank <User> <Rank>"); 

  			  return true;
  		  
  			  } 
  		  
  		  else {

  	
  		    
  		  	for (String test : acceptedranks) {
  		  		
  		  		if (args[1].toLowerCase().startsWith(test)) {
  		  			
  		  			if (!(Main.mysql.getRank(args[0]).equalsIgnoreCase(""))) {
  		  		
  		  				Main.mysql.updateRank(args[0], test);
  		  			
  		  			} else {
  		  				
  		  				Main.mysql.addRank(args[0], test);

  		  			}
    	    		 
  		  			Player target = Main.getPlugin().getServer().getPlayer(args[0]);
    	    		  
    	    		  if (target != null) {
    	    			  
    	    			  com.nametagedit.plugin.NametagEdit.getApi().setNametag(target, Main.mysql.getPrefix(target.getName()), "");
      				  
    	    			  target.setPlayerListName("§7" + Main.mysql.getPrefix(target.getName()) + target.getName());
    	    		
    	    			  target.setScoreboard(Board.getboard(target.getName(), true));
    	    			  
    	    			  sender.sendMessage(ChatColor.RED + "Rank> " + ChatColor.GRAY + args[0] + "'s rank has been set to: " + ChatColor.RED + args[1]); 
    	    			  return true;
    	    		  }
    	    		  
    	    
    	    		if (sender instanceof Player) {
    	    	    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                	out.writeUTF("KickPlayer");
                	out.writeUTF(args[0]);
                	out.writeUTF(ChatColor.RED + "Your rank has been updated from another bungee instance! As a safety measure you have been kicked from the server.\n" + ChatColor.GRAY + "Please reconnect.");
                	Player player = (Player) sender;
                	
                	Main.sendPluginMsg(player, out);
       
    	    		  
    	    	    sender.sendMessage(ChatColor.RED + "Rank> " + ChatColor.GRAY + args[0] + "'s rank has been set to: " + ChatColor.RED + args[1]); 
    	    	    return true;
    	    	
    	    		} else {
    	    
    	    		  sender.sendMessage(ChatColor.RED + "\nRank> " + ChatColor.GRAY + "UH OH! You're using the console, therefore BYPASSING the kick SAFETY measure put in place to solve nametag and scoreboard issues. Please note that this may cause issues with player scoreboards and nametags! \n");
    	    		  return true; 
    		       	}
  		  		
  		  		}
  		  	}
  		  		
  		  sender.sendMessage(ChatColor.RED + "Rank> " + ChatColor.GRAY + args[1] + " is not a VALID rank."); 

  		       	
  		   
  		 
  	  } 
  	  } else {
  	  	
  		  
  		   sender.sendMessage(ChatColor.RED + "Rank> " + ChatColor.GRAY + "You must be " + ChatColor.DARK_RED + ChatColor.BOLD + "H.O.S." + ChatColor.GRAY + " or higher to run this commmand!");
  		   return true;    
 		  
 	  }
      		  	
  	  }
  	  
  	  return true;
	}
}
