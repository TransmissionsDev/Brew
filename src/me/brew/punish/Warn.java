package me.brew.punish;

import org.bukkit.command.Command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;

import org.bukkit.command.CommandExecutor;

public class Warn implements Listener, CommandExecutor  {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

	     if (cmd.getName().equalsIgnoreCase("warn") && Main.getPlugin().getConfig().getBoolean("warn-module")) { 
	        		  if (Main.mysql.modOrHigher(sender.getName()) || !(sender instanceof Player)) {
	                if (args.length == 0) {
	                    sender.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You must specify a user to warn! Ex: " + ChatColor.RED + "/warn <User> <Reason>"); 

	                    return true;

	                } else {


	                    Player target = Main.getPlugin().getServer().getPlayer(args[0]);

	                    if (target == null) {

	                        sender.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You did not specify an" + ChatColor.RED + "online player" + ChatColor.GRAY + "!");

	                        return true;

	                    } else {



	                        if (args.length > 1) {
	                            ArrayList < String > al = new ArrayList < String > ();

	                            for (String p: args) {
	                                al.add(p);


	                            }        
	                        


	                            al.remove(0);

	                            String reason = "";

	                            for (String p: al) {

	                                reason = reason + " " + p;

	                            }


	                            sender.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You have gave a warning to: " + ChatColor.RED + target.getName() + ChatColor.GRAY + " with the reason:" + ChatColor.RED + reason);
	                            target.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You have been been given a warning. Reason:" + ChatColor.RED + reason);
	                            return true;
	                        }

	                        sender.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You gave a warning to: " + ChatColor.RED + target.getName());
	                        target.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You have been been given a warning!");
	                        return true;
	                    }


	                }


	            } 	
	            
	            	else {
	          	  
	            		 sender.sendMessage(ChatColor.RED + "Warn> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
	                 	return true;
	            }
	            
	        }
	    
		
		return true;
		
	}
	}
