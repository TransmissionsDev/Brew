package me.brew.punish;

import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class Ban implements Listener, CommandExecutor  {

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	      if (cmd.getName().equalsIgnoreCase("ban")) { 
	        	if (sender instanceof Player) {
	        	if (Main.getPlugin().getConfig().getBoolean("ban-module")) {
	        		  if (Main.mysql.modOrHigher(sender.getName())) {



	                if (args.length == 0) {
	                    sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "You must specify a user to ban! Ex: " + ChatColor.RED + "/ban <User> <Reason>"); 

	                    return true;

	                } else {



	                  

	                  

	                        if (args.length > 1) {
	                            ArrayList < String > al = new ArrayList < String > ();

	                            for (String p: args) {
	                                al.add(p);


	                            }


	                            al.remove(0);

	                            String reason = "";

	                            for (String p: al) {

	                                reason = reason + " " + p;

	                            }


	                        
	    
	                            if (Main.mysql.getBanType(args[0]) == null) {
	                            	
	                            	Main.mysql.banPlayer(sender.getName(), args[0], reason);
	                               	
	                               	sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "You have banned: " + ChatColor.RED + args[0] + ChatColor.GRAY + " with the reason:" + ChatColor.RED + reason); 

	                           	
	                            	
	                               	try {
	                               		Player player = (Player) sender;
	                               		for (Player p : Bukkit.getOnlinePlayers()) {
	                                       	
	                              	       
	                               		  
	                               		 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                               				
	                               				
	                               			
	                               				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has kicked");
	                               				
	                               				} 
	                                		 }
	                                  
	                               		  
	                                 
	                                      
	                            		
	                      
	                               		
	                          
	                                	ByteArrayDataOutput out = ByteStreams.newDataOutput();
	                                	out.writeUTF("KickPlayer");
	                                	out.writeUTF(args[0]);
	                                	out.writeUTF(ChatColor.RED + "You have been PERMANTLY banned from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason);
	                                	
	                                	
	                                 	Main.sendPluginMsg(player, out);
	                       
	                                	
	                            	 
	                            	 
	                                return true;
	                               	
	                                
	                                
	                                
	                               	} 
	                              
	                               	catch (Throwable e) {
	                               		
	                                	StringWriter sw = new StringWriter();
	                                	e.printStackTrace(new PrintWriter(sw));
	                                	String exceptionAsString = sw.toString();
	                                	
	                               		sender.sendMessage(ChatColor.RED + "Uh oh, error! Contact a network administrator immediately! Stack trace:\n" + exceptionAsString);

	                               		
	                               		return true;

	                               		
	                               		
	                                }
	                                 
	                                 
	                               	
	                                  
	                            
	                                    
	                            }
	                            	

	                     
	                            
	                            
	                          
	                            
	                            
	                     	   sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + args[0] + " is already banned!");

	                           return true;

	                            
	                         
	                            
	                            
	                            
	                          
	                         

	                        }

	                        if (Main.mysql.getBanType(args[0]) == null) {
	                        	
	                        	Main.mysql.banPlayer(sender.getName(), args[0], "");
	                           	
	                           	sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "You have banned: " + ChatColor.RED + args[0]); 
	                       	
	                        	

	                           	try {
	                           		
	                           	
	                           		
	                       
	                        	ByteArrayDataOutput out = ByteStreams.newDataOutput();
	                        	out.writeUTF("KickPlayer");
	                        	out.writeUTF(args[0]);
	                        	out.writeUTF(ChatColor.RED + "You have been PERMANTLY banned from this server!\n\n" + ChatColor.GRAY + "No reason given.");
	                        	Player player = (Player) sender;
	                        	
	                        	Main.sendPluginMsg(player, out);
	               
	                        	for (Player p : Bukkit.getOnlinePlayers()) {
	                               	
	                      	       
	                       		  
	                        		 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
	                       				
	                       			
	                       				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has banned " + ChatColor.RED + args[0]);
	                       				
	                       				} 
	                        		
	                          
	                       		  
	                         
	                             }    
	                    		
	              
	                        	
	                            return true;
	                           
	                           	} 
	                            catch (Throwable e) {
	                           		
	                            	StringWriter sw = new StringWriter();
	                            	e.printStackTrace(new PrintWriter(sw));
	                            	String exceptionAsString = sw.toString();
	                            	
	                           		sender.sendMessage(ChatColor.RED + "Uh oh, error! Contact a network administrator immediately! Stack trace:\n" + exceptionAsString);

	                           		
	                           		return true;

	                           		
	                            }
	                             
	                              
	                        
	                                
	                        }
	                        	

	                 
	                        
	                        
	                      
	                        
	                        
	                 	   sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + args[0] + " is already banned!");

	                       return true;

	                        

	                }

	            	}
	              	
	            	else {
	                	  
	            		 sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
	                	  return true;
	                  }
	            	
	            }
	        }
	        
	        else {
	        	 sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "Due to bungeecord limitations, the console can't ban!");
	        	  return true;
	        	
	        }
	        
	        }



		
		return true;
		
	}
	}
