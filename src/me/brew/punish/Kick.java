package me.brew.punish;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.util.ArrayList;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class Kick implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
	  
    	if (cmd.getName().equalsIgnoreCase("kick")) { 
			
    		if (Main.mysql.modOrHigher(sender.getName()) || !(sender instanceof Player)) {
    	
    		
    			if (args.length == 0) {
    				
    				 sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You must be specify a player to kick or @everyone, which will kick everyone on the server execpt you! EG: " + ChatColor.RED + "/kick <Player or @everyone> ");
        	         return true;
    				
    			} 

    			if (args.length > 1) {
    				
    				ArrayList < String > al = new ArrayList < String > ();

    	               for (String p: args) {
    	                   al.add(p);


    	               }        
    	           
    	               al.remove(0);

    	           

    	               String reason = "";

    	               for (String p: al) {
    	            	
    	            	   
    	            	   reason = reason + " " + p;
    	            	   

    	               }

    				if (args[0].equalsIgnoreCase("@everyone")) {
    					if (sender instanceof Player) {
    						Player player = (Player) sender;
    						for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
        						if (p.getName() != player.getName()) {
        							
        							
        					
        							
        							p.kickPlayer(ChatColor.RED + "You have been kicked from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason);
        							
        						}	
        					}
        				
        					 sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You have kicked everyone from the server!");
                	         return true;
        					
        				} else {
        					
        					for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
        						
        							
        						p.kickPlayer(ChatColor.RED + "You have been kicked from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason);
    								
        						
        						
        					}
        				
        					 sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You have kicked everyone from the server!");
                	         return true;
        					
        				}
    					}
    					
    				
    				Player target = Main.getPlugin().getServer().getPlayer(args[0]);
    				
    				if (target == null) {
    					 
    					sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + args[0] + " is not an online player!");
            	         return true;
    					
    				} else {
    					
    					for (Player p : Bukkit.getOnlinePlayers()) {
                           	
                   	       
                     		  
                     		  if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
                     				if (sender instanceof Player) {
                     					Player player = (Player) sender;
                     					
                     					ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has kicked " + ChatColor.RED + args[0]);
                     			
                     				} else {
                     				
                     					ActionBarAPI.sendActionBar(p, ChatColor.GRAY + "The CONSOLE has kicked " + ChatColor.RED + args[0]);
                     					
                     				}	 
                      		 }
                        }    
                  		
    					
    					target.kickPlayer(ChatColor.RED + "You have been kicked from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason);
    					sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You have kicked: " + target.getPlayerListName() + ChatColor.GRAY + " from the server!");

    					return true;
    					
    					
    				}
    					
    				
    				
    			}
    			
    			
    			
    			if (args.length == 1) {
    				
    				if (args[0].equalsIgnoreCase("@everyone")) {
    					if (sender instanceof Player) {
    					
    						Player player = (Player) sender;
    						
    						for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {
        						if (p.getName() != player.getName()) {
        							p.kickPlayer(ChatColor.RED + "You have been kicked from this server!\n\n" + ChatColor.GRAY + "No Reason Specified.");
        							
        						}
        					}
        				
        					 sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You have kicked everyone from the server!");
                	         return true;
        					
        				} else {
        					
        					for (Player p : Main.getPlugin().getServer().getOnlinePlayers()) {	
        						p.kickPlayer(ChatColor.RED + "You have been kicked from this server!\n\n" + ChatColor.GRAY + "No Reason Specified.");
        					}
        					
        					for (Player p : Bukkit.getOnlinePlayers()) {
        						 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
                       				if (sender instanceof Player) {
                       				Player player = (Player) sender;
                       			
                       				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has kicked " + ChatColor.RED + "everyone");
                       				
                       				} else {
                       					
                       					ActionBarAPI.sendActionBar(p, ChatColor.GRAY + "The CONSOLE has kicked " + ChatColor.RED + "everyone");

                       				} 
                        		}
                            }    
                    		
        				
        					 sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You have kicked everyone from the server!");
                	         return true;
        					
        				}	
    					}
    					
    				
    				Player target = Main.getPlugin().getServer().getPlayer(args[0]);
    				
    				if (target == null) {
    					 
    					sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + args[0] + " is not an online player!");
            	         return true;
    					
    				} else {
    					
    					for (Player p : Bukkit.getOnlinePlayers()) {
    						 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
                     				if (sender instanceof Player) {
                     					Player player = (Player) sender;
                     					
                     					ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has kicked " + ChatColor.RED + args[0]);
                     				
                     				} else {
                     				
                     					ActionBarAPI.sendActionBar(p, ChatColor.GRAY + "The CONSOLE has kicked " + ChatColor.RED + args[0]);

                     				}
    						 }
                        }    
    					
    					target.kickPlayer(ChatColor.RED + "You have been kicked from this server!\n\n" + ChatColor.GRAY + "No Reason Specified.");
						
    					sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You have kicked: " + target.getPlayerListName() + ChatColor.GRAY + " from the server!");
    					return true;
    					
    					
    				}
    				
    			}
    				
    				
    			} else {
    			
    			  sender.sendMessage(ChatColor.RED + "Kick> " + ChatColor.GRAY + "You must be a" + ChatColor.DARK_GREEN + ChatColor.BOLD + "MOD" + ChatColor.GRAY + " or higher to run this commmand!");
    	           return true; 
    		}
    		
    	}
        
		
		return true;
		
	}
	}
