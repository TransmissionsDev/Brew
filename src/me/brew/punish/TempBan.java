package me.brew.punish;

import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.bukkit.*;
import org.bukkit.command.CommandExecutor;

public class TempBan implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		
	    if (cmd.getName().equalsIgnoreCase("tempban")) { 
        	if (sender instanceof Player) {
        	if (Main.getPlugin().getConfig().getBoolean("tempban-module")) {
        		  if (Main.mysql.helperOrHigher(sender.getName())) {



                if (args.length == 0) {
                    sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "You must specify a user to tempban! Ex: " + ChatColor.RED + "/tempban <User> <Days> <Hours> <Reason>"); 

                    return true;
                    
                    
                

                } else {

                	if (args.length == 2) {
                		 sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "You must specify the amount of days and hours you want to tempban for! (Can be 0!) Ex: " + ChatColor.RED + "/tempban <User> <Days> <Hours> <Reason>"); 
                		 return true;
                	}
                	
                    if (args.length == 1) {
               		 sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "You must specify the amount of days and hours you want to tempban for! (Can be 0!) Ex: " + ChatColor.RED + "/tempban <User> <Days> <Hours> <Reason>"); 

               		 return true;
                    }


                 
                    if (Main.mysql.getBanType(args[0]) == null) {
                        if (args.length > 3) {
                        	
                        	
                        	ArrayList < String > al = new ArrayList < String > ();

                            for (String p: args) {
                                al.add(p);


                            }


                            al.remove(0);
                            al.remove(0);
                            al.remove(0);
                  

                            String reason = "";

                            for (String p: al) {

                                reason = reason + " " + p;

                            }

                            int DaysPre;

                            try {
                                DaysPre = Integer.parseInt(args[1]);
                            } catch (NumberFormatException e) {
                                sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "The days field can't contain any characters other than numbers!"); 
                                return true;
                            }
                            
                            int HoursPre;
                            
                            try {
                            	HoursPre = Integer.parseInt(args[2]);
                            } catch (NumberFormatException e) {
                                sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "The hours field can't contain any characters other than numbers!"); 
                                return true;
                            }

                            if (DaysPre == 0 && HoursPre == 0) {

                                sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "Both fields can't be 0!"); 
                                return true;
                            }

                            if (HoursPre > 23) {
                            	
                            	sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "The hours field can't be over 23!"); 
                            	return true;
                            }
                            
                           
                            long Days = TimeUnit.DAYS.toMillis(DaysPre);
                            
                            long Hours = TimeUnit.HOURS.toMillis(HoursPre); 
        
                            long FinalTime = System.currentTimeMillis() + Days + Hours;  
                            
                            Date time = new Date(FinalTime);
                            
                            Main.getPlugin().getServer().getLogger().info(FinalTime + ""); 


                            
                            Main.mysql.tempbanPlayer(sender.getName(), args[0], reason, FinalTime);

                            
                     
  
                            
                            sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "You have tempbanned: " + ChatColor.RED + args[0] + ChatColor.GRAY + " with the reason:" + ChatColor.RED + reason + ChatColor.GRAY + " for: " + ChatColor.RED + DaysPre + ChatColor.GRAY + " day(s) and: " + ChatColor.RED + HoursPre + ChatColor.GRAY + " hour(s)!"); 

                        
                           	try {
                               
                           		
                           		
                           		ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            	out.writeUTF("KickPlayer");
                            	out.writeUTF(args[0]);
                            	
                            	
                            	out.writeUTF(ChatColor.RED + "You have been temporarily banned from this server!\n\n" + ChatColor.GRAY + "Reason:" + ChatColor.WHITE + reason + ChatColor.GRAY + "\nUnban Date: " + ChatColor.AQUA + ChatColor.UNDERLINE + time.toString());

                            	Player player = (Player) sender;
                            	
                          
                            	for (Player p : Bukkit.getOnlinePlayers()) {
                                   	
                           	       
                             		  
                             		  if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
                             		
                             				
                             			
                             				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has temp-banned " + ChatColor.RED + args[0]);
                             				
                             				} 
                              		 
                                
                             		  
                               
                                   }    
                          		
                            	
                            	Main.sendPluginMsg(player, out);
                   
                               return true;
                               	} 
                           	
                                catch (Throwable e) {
                               		
                                	StringWriter sw = new StringWriter();
                                	e.printStackTrace(new PrintWriter(sw));
                                	String exceptionAsString = sw.toString();
                                	
                               		sender.sendMessage(ChatColor.RED + "Uh oh, error! Contact a network administrator immediately! Stack trace:\n" + exceptionAsString);

                               		
                               		return true;

                               		
                                }
                           
                        	
                            
                        	  
                   

                        

                        }


                        int DaysPre;

                        try {
                            DaysPre = Integer.parseInt(args[1]);
                        } catch (NumberFormatException e) {
                            sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "The days field can't contain any characters other than numbers!"); 
                            return true;
                        }
                        
                        int HoursPre;
                        
                        try {
                        	HoursPre = Integer.parseInt(args[2]);
                        } catch (NumberFormatException e) {
                            sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "The hours field can't contain any characters other than numbers!"); 
                            return true;
                        }

                        if (DaysPre == 0 && HoursPre == 0) {

                            sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "Both fields can't be 0!"); 
                            return true;
                        }

                        
                        if (DaysPre > 23) {
                        	
                        	sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "The hours field can't be over 23!"); 
                        	return true;
                        }
                        
                        
                        long Days = TimeUnit.DAYS.toMillis(DaysPre);
                        
                        long Hours = TimeUnit.HOURS.toMillis(HoursPre); 
                       
                     
          
                        
                        
                        long FinalTime = System.currentTimeMillis() + Days + Hours;
                        
                        Main.getPlugin().getServer().getLogger().info(FinalTime + ""); 
                        
                        
                        Main.mysql.tempbanPlayer(sender.getName(), args[0], "", FinalTime);

                        Date time = new Date(FinalTime);
                        

                        sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "You have tempbanned: " + ChatColor.RED + args[0] + ChatColor.GRAY + " for: " + ChatColor.RED + DaysPre + ChatColor.GRAY + " day(s) and: " + ChatColor.RED + HoursPre + ChatColor.GRAY + " hour(s)!"); 

                  

                        
                       	try {
                     
                        	ByteArrayDataOutput out = ByteStreams.newDataOutput();
                        	out.writeUTF("KickPlayer");
                        	out.writeUTF(args[0]);
                        	
                        	
                        	out.writeUTF(ChatColor.RED + "You have been temporarily banned from this server!\n" + ChatColor.GRAY + "\nUnban Date: " + ChatColor.AQUA + ChatColor.UNDERLINE + time.toString());

                        	Player player = (Player) sender;
                        	
                        	for (Player p : Bukkit.getOnlinePlayers()) {
                               	
                       	       
                         		  
                        		 if (Main.mysql.adminOrHigher(p.getName()) && Main.getPlugin().getConfig().getBoolean("actionbar-logs")) {
                         				
                         			
                         				ActionBarAPI.sendActionBar(p, player.getPlayerListName() + ChatColor.GRAY + " has temp-banned " + ChatColor.RED + args[0]);
                         				
                         				} 
                          		 
                            
                         		  
                           
                               }    
                      		
                        	
                        	Main.sendPluginMsg(player, out);
               
                           return true;
                           	} 
                       	
                            catch (Throwable e) {
                           		
                            	StringWriter sw = new StringWriter();
                            	e.printStackTrace(new PrintWriter(sw));
                            	String exceptionAsString = sw.toString();
                            	
                           		sender.sendMessage(ChatColor.RED + "Uh oh, error! Contact a network administrator immediately! Stack trace:\n" + exceptionAsString);

                           		
                           		return true;

                           		
                            }



                }
                    
                    else {
                		 
              		  sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + args[0] + " is already banned!");

                        return true;

                        
              		  
              	  }
                
                }
                
            	
                
            	 


            	}
              	
            	else {
                	  
                	  sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "You must be a " + ChatColor.DARK_AQUA + ChatColor.BOLD + "HELPER" + ChatColor.GRAY + " or higher to run this commmand!");
                	  return true;
                  }

            }
           
        }
        	 else {
               	 sender.sendMessage(ChatColor.RED + "TempBan> " + ChatColor.GRAY + "Due to bungeecord limitations, the console can't ban!");
               	  return true;
               	
               }
        	
        }
		
		return true;
		
	}
	}
