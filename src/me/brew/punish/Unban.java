package me.brew.punish;


import org.bukkit.command.Command;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import me.brew.Main;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.CommandExecutor;

public class Unban implements Listener, CommandExecutor  {


	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		  
		if (cmd.getName().equalsIgnoreCase("unban")) { 
		  		if (Main.mysql.adminOrHigher(sender.getName()) || !(sender instanceof Player)) {
		  			if (args.length == 0) {
		  			  sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "You must specify a player to unban! Ex: " + ChatColor.RED + "/unban <User>"); 
		 
		  			  return true;
		  			 
		  			}
		  		  
		  		  if (Main.mysql.getBanType(args[0]) != null) {
		  			
		  			 Main.mysql.unbanPlayer(args[0]);
		  			 sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + args[0] + " has been unbanned!");
		  			 return true;
		  			  
		  			  
		  		  } 
		  		  
		  		  else {
		  		
		  			  sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + args[0] + " is not currently banned!"); 
		  			  return true;
		  		  }

		  		  
		  		  
		  	  } else {
		  		  
		  		  sender.sendMessage(ChatColor.RED + "Punish> " + ChatColor.GRAY + "You must be an " + ChatColor.DARK_RED + ChatColor.BOLD + "ADMIN" + ChatColor.GRAY + " or higher to run this commmand!");
		      	  return true;  
		  	  }
		  	 
		  	  }
		  	  
		return true;
		
	}
	}
